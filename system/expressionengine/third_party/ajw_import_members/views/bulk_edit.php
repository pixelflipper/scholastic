<?php echo form_open( $bulk_edit_action ); ?>
<?php echo form_hidden('confirm', 'confirm'); ?>
<?php echo form_hidden('action', $action); ?>
<?php foreach($toggle as $item): ?>
	<?php echo form_hidden('toggle[]', $item); ?>
<?php endforeach;?>

<p class="shun"><?php echo $confirm_message; ?></p>

<p class="notice"><?php echo lang('action_can_not_be_undone'); ?></p>

<p>
	<?php echo 
		form_submit(
			array('name' => 'submit', 'value' => lang('submit'), 'class' => 'submit')
		);
	?>
</p>

<?php echo form_close()?>