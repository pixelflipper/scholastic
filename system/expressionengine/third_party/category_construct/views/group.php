<?php if (! $settings['license_key']) { ?>
	<p class="construct-no-license">
		<?php echo lang('no_license_key'); ?>
	</p>
<?php } ?>

<meta data-set="licenseKey" data-value="<?php echo $settings['license_key']; ?>">
<meta data-set="phoneHome" data-value="<?php if ($settings['phone_home']) { echo 'true'; } else { echo 'false'; } ?>">
<?php // Set data for the JS functions ?>
<meta data-set="baseUrl" data-value="<?php echo $base_url; ?>">
<meta data-set="groupId" data-value="<?php echo $category_group['group_id']; ?>">
<meta data-set="addCatOverlayUrl" data-value="<?php echo $base_url; ?>&amp;method=add_cat_overlay">
<meta data-set="addCatUrl" data-value="<?php echo $base_url; ?>&amp;method=insert_cat&amp;group=<?php echo $category_group['group_id']; ?>">
<meta data-set="deleteCatUrl" data-value="<?php echo $base_url; ?>&amp;method=delete_cat">
<meta data-set="updateCatUrl" data-value="<?php echo $base_url; ?>&amp;method=update_cat">
<?php // Set a parent element for JS Watching ?>
<div class="js-construct-watch">
	<div class="construct">
		<?php // Ordered list for our items ?>
		<ol class="construct__cats construct__cats--transparent js-construct-cats js-construct-sortable">
			<?php // Loop through our categories ?>
			<?php foreach ($categories as $category) { ?>
				<?php // Category View ?>
				<?php echo ee()->load->view('category', $category, true); ?>
			<?php } ?>
		</ol>

		<?php // If no categories, show the notice ?>
		<p class="construct-notice <?php if ($categories) { ?> construct-notice--is-hidden<?php } ?> js-no-cats">
			<?php echo lang('no_categories_in_group'); ?>
		</p>

		<?php // Add Category ?>
		<div class="construct__add-cat">
			<a
				class="construct__add-cat-button js-construct-add-cat"
				style="background-image:url(<?php echo $themes_url; ?>construct-sprite.png)"
				data-action="<?php echo $base_url; ?>&amp;method=add_cat_overlay"
			>
				Add Category
			</a>
		</div>
	</div>
</div>