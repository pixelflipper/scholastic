<?php 
	echo form_open( $form_action ); 
	echo form_hidden( "step", "settings" ); 
?>

<h3>Datatype settings</h3>

<?php 

$this->table->set_template($cp_table_template);
$this->table->set_heading('Setting', 'Value');
echo $this->table->generate($settings);

?>

<input type="submit" value="Check settings" class="submit" />

<?php echo form_close(); ?>
