<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * AJW Simple Module Install/Update File
 *
 * @package	ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author	Andrew Weaver
 * @link	http://brandnewbox.co.uk/
 */

class Ajw_import_members_upd {
	
	public $version = '1.0';
	
	private $module_name = "Ajw_import_members";
	private $EE;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->EE =& get_instance();
	}
	
	/**
	 * Installation Method
	 *
	 * @return 	boolean 	TRUE
	 */
	public function install() {
		
		$mod_data = array(
			'module_name' => $this->module_name,
			'module_version' => $this->version,
			'has_cp_backend' => "y",
			'has_publish_fields' => 'n'
		);
		$this->EE->db->insert('modules', $mod_data);
		
		$this->EE->load->dbforge();
		
		// Add an action to call imports from templates
		$data = array(
			'class' 	=> 'Ajw_import_members',
			'method' 	=> 'run_action'
		);
		$this->EE->db->insert('actions', $data);

		// Create table for saved imports
		$fields = array(
			'id' => array(
				'type' => 'int',
				'constraint' => '6',
				'unsigned' => TRUE,
				'auto_increment'=> TRUE
			),
			'name' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'null' => FALSE
			), 
			'description' => array(
				'type' => 'text',
				'null' => FALSE
			), 
			'passkey' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'default' => ''
			), 
			'settings' => array(
				'type' => 'text'
			), 
			'last_run' => array(
				'type' => 'int',
				'constraint' => '10',
				'unsigned' => TRUE,
				'default' => 0,
			)
		);

		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('id', TRUE);
		$this->EE->dbforge->create_table('ajw_import_members');		
		return TRUE;
	}

	// ----------------------------------------------------------------
	
	/**
	 * Uninstall
	 *
	 * @return 	boolean 	TRUE
	 */	
	public function uninstall() {
		
		$this->EE->db->select('module_id');
		$query = $this->EE->db->get_where('modules', array('module_name' => $this->module_name));
		
		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');
		
		$this->EE->db->where('module_name', $this->module_name);
		$this->EE->db->delete('modules');
		
		$this->EE->db->where('class', $this->module_name);
		$this->EE->db->delete('actions');
		
		$this->EE->db->where('class', $this->module_name.'_mcp');
		$this->EE->db->delete('actions');
		
		$this->EE->load->dbforge();
		$this->EE->dbforge->drop_table('ajw_import_members');
		
		return TRUE;
	}
	
	// ----------------------------------------------------------------
	
	/**
	 * Module Updater
	 *
	 * @return 	boolean 	TRUE
	 */	
	public function update($current = '') {

		if ($current == $this->version) {
			// No updates
			return FALSE;
		}

		// New version - update any tables

		/*
		$this->EE->load->dbforge();
		if ( $current < "1.0" )  {
			// Update database if necessary
		}
		*/

		return TRUE;
	}
	
}
/* End of file upd.ajw_import_members.php */
/* Location: /system/expressionengine/third_party/ajw_import_members/upd.ajw_import_members.php */