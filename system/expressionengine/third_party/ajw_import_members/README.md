# AJW Import Members

Import Members is an EE2 module to import members from CSV or XML files. 

Features:

* Import to default and custom member fields
* Add members to different member groups
* Generate passwords, or use existing MD5/SHA1 ones
* Save and re-run imports

## Installation

1. Download and extract the add-on
2. Copy the folder to your system/expressionengine/third_party folder
3. In the Control Panel, go to Add-Ons > Modules and click on install

## Screen shots

[Configure Import](https://dl.dropbox.com/u/179784/Screenshots/ConfigureMemberImport.png)
