$(document).ready(function()
{
	$('#escort').sortable({
		axis: 'y',
		containment: $('#mainContent'),
		handle: 'thead',
		items: 'table',
		start: function(event, ui)
		{
			$('#escort tbody').fadeTo(0, 0.5);
		},
		stop: function(event, ui)
		{
			$('#escort tbody').fadeTo(0, 1);
		}
	});
});