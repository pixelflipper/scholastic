<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Category Construct Model
 *
 * @package    category_construct
 * @author     TJ Draper <tj@buzzingpixel.com>
 * @link       https://buzzingpixel.com/ee-add-ons/category-construct
 * @copyright  Copyright (c) 2015, BuzzingPixel
 */
class Category_construct_model extends CI_Model {

	/**
	 * Get Settings
	 *
	 * @access      public
	 * @return      array
	 */
	public function getSettings()
	{
		// Query the settings from the database
		$query = ee()->db
			->select('*')
			->from('category_construct_settings')
			->get();

		$queryResult = $query->result_array();

		// Format the settings
		$settings = array();

		foreach ($queryResult as $key => $value) {
			$settings[$value['settings_key']] = $value['settings_value'];
		}

		// Check if we need to phone home
		if ($settings['phone_home'] < time()) {
			$settings['phone_home'] = true;

			$this->_incrementPhoneHome();
		} else {
			$settings['phone_home'] = false;
		}

		// Return the settings
		return $settings;
	}

	public function setLicenseKey($key)
	{
		ee()->db->where('settings_key', 'license_key');
		ee()->db->update('category_construct_settings', array(
			'settings_value' => $key
		));
	}

	private function _incrementPhoneHome()
	{
		ee()->db->where('settings_key', 'phone_home');
		ee()->db->update('category_construct_settings', array(
			'settings_value' => strtotime('+1 day', time())
		));
	}

	/**
	 * Get Category Groups
	 *
	 * @access      public
	 * @param       array
	 * @return      array
	 */
	public function getCategoryGroups($conf = array())
	{
		$defaultConf = array(
			'groupId' => false,
			'groupName' => false,
			'singleResult' => false
		);

		$conf = array_merge($defaultConf, $conf);

		ee()->db
			->select('*')
			->from('category_groups')
			->order_by('group_name', 'asc');

		if ($conf['groupId']) {
			ee()->db->where_in('group_id', $conf['groupId']);
		}

		if ($conf['groupName']) {
			ee()->db->where_in('group_name', $conf['groupName']);
		}

		if ($conf['singleResult']) {
			ee()->db->limit(1);
		}

		$query = ee()->db->get();

		if ($conf['singleResult']) {
			return $query->row_array();
		} else {
			return $query->result_array();
		}
	}

	/**
	 * Set All Category Groups to Custom Sorting
	 *
	 * @access      public
	 * @return      bool
	 */
	public function setCustomSorting()
	{
		$categoryGroups = $this->getCategoryGroups();

		if ($categoryGroups) {
			$data = array();

			foreach ($categoryGroups as $key => $val) {
				$data[$key]['group_id'] = $val['group_id'];
				$data[$key]['sort_order'] = 'c';
			}

			ee()->db->update_batch('category_groups', $data, 'group_id');

			return true;
		}

		return false;
	}

	/**
	 * Get Categories
	 *
	 * @access      public
	 * @param       array
	 * @return      array
	 */
	public function getCategories($conf = array())
	{
		$defaultConf = array(
			'catId' => false,
			'groupId' => false,
			'maxDepth' => false,
			'parentId' => false,
			'entryId' => false,
			'showEmpty' => true,
			'catName' => false,
			'catSlug' => false,
			'orderBy' => false,
			'sort' => 'asc',
			'limit' => false,
			'singleRow' => false,
			'nest' => true,
			'customFields' => false
		);

		$conf = array_merge($defaultConf, $conf);

		$this->maxDepth = $conf['maxDepth'];

		ee()->db
			->select('C.*')
			->from('categories C');

		if ($conf['catId']) {
			ee()->db->where_in('C.cat_id', $conf['catId']);
		}

		if ($conf['groupId']) {
			ee()->db->where_in('C.group_id', $conf['groupId']);
		}

		if ($conf['parentId']) {
			ee()->db->where_in('C.parent_id', $conf['parentId']);
		}

		if ($conf['entryId'] or ! $conf['showEmpty']) {
			ee()->db
				->join('category_posts CP', 'C.cat_id = CP.cat_id')
				->group_by('C.cat_id');
		}

		if ($conf['entryId']) {
			ee()->db->where_in('CP.entry_id', $conf['entryId']);
		}

		if ($conf['catName']) {
			ee()->db->where_in('C.cat_name', $conf['catName']);
		}

		if ($conf['catSlug']) {
			ee()->db->where_in('C.cat_url_title', $conf['catSlug']);
		}

		if ($conf['orderBy']) {
			ee()->db->order_by('C.' . $conf['orderBy'], $conf['sort']);
		} else {
			ee()->db->order_by('C.parent_id ASC, C.cat_order ASC');
		}

		if ($conf['singleRow']) {
			ee()->db->limit(1);
		} elseif ($conf['limit']) {
			ee()->db->limit($conf['limit']);
		}

		if ($conf['customFields']) {
			ee()->db
				->select('CFD.*')
				->join('category_field_data CFD', 'C.cat_id = CFD.cat_id');
		}

		$query = ee()->db->get();

		$returnData = $query->result_array();

		if ($conf['customFields']) {
			$catFieldsQuery = ee()->db
				->select('field_id, field_name')
				->from('category_fields')
				->get();

			$catFields = $catFieldsQuery->result_array();

			ee()->load->library('typography');
			ee()->typography->initialize();

			foreach ($returnData as $key => $val) {
				foreach ($catFields as $fKey => $fVal) {
					$typographyPrefs = array(
						'text_format' => $val['field_ft_' . $fVal['field_id']],
						'html_format' => 'all',
						'auto_links' => 'n',
						'allow_img_url' => 'y'
					);

					$val[$fVal['field_name']] = ee()->typography->parse_type(
						$val['field_id_' . $fVal['field_id']],
						$typographyPrefs
					);

					unset($val['field_id_' . $fVal['field_id']]);
					unset($val['field_ft_' . $fVal['field_id']]);

					$returnData[$key] = $val;
				}
			}
		}

		if ($conf['singleRow']) {
			return $returnData[0];
		} elseif ($conf['nest']) {
			$this->rawCategories = $returnData;
			$this->sortedCategories = array();

			// Assign cat id as key
			$keyedCats = array();

			foreach ($this->rawCategories as $key => $val) {
				$keyedCats[$val['cat_id']] = $val;
			}

			$this->rawCategories = $keyedCats;

			$this->_sortAndNest();

			$this->_setUrlPathVar();

			return $this->sortedCategories;
		} else {
			return $returnData;
		}
	}

	/**
	 * Set Category Sorting and Nesting
	 *
	 * @access      private
	 * @param       int
	 * @return      bool
	 */
	private function _sortAndNest($level = 0)
	{
		// Set vars
		$level++;
		$return = true;

		// If this is the first level
		if ($level === 1) {
			// Set level vars
			$count = 1;
			$counter = array();

			// Do not return
			$return = false;

			// Get the first parent ID
			foreach ($this->rawCategories as $key => $val) {
				$firstParentId = $val['parent_id'];

				break;
			}

			// Set all first level cats
			foreach ($this->rawCategories as $key => $val) {
				// Make sure this is our first level category
				if ($val['parent_id'] == $firstParentId) {
					// Set the item to the sorted categories array
					$this->sortedCategories[$key] = $val;

					// Set the level values
					$this->sortedCategories[$key]['cat_level'] = $level;
					$this->sortedCategories[$key]['level_count'] = $count;

					// Put this item in the counter so we can count it later
					$counter[$key] = $key;

					// Remove the processed category from the raw categories
					unset($this->rawCategories[$key]);

					// Increment the count for the loop
					$count++;
				}
			}

			// Set level total results
			$total = count($counter);
			foreach ($this->sortedCategories as $key => $val) {
				$this->sortedCategories[$key]['level_total_results'] = $total;
			}
		} else {
			// Start a new sort array
			$newSort = array();

			// Go through each of the already sorted categories
			foreach ($this->sortedCategories as $key => $val) {
				// Set it to the new sort array
				$newSort[$key] = $val;

				// Reset the count variable
				$count = 1;

				// Start a counter array
				$counter = array();

				// Loop through the raw categories
				foreach ($this->rawCategories as $rawKey => $rawVal) {
					// If it's parent is in the array, add it here as a child
					if ($rawVal['parent_id'] == $key) {
						// We have results so do not return (must go recursive)
						$return = false;

						// Set the item to the sorted categories array
						$newSort[$rawKey] = $rawVal;

						// Set the level values
						$newSort[$rawKey]['cat_level'] = $level;
						$newSort[$rawKey]['level_count'] = $count;

						// Put this item in the counter so we can count it later
						$counter[$rawKey] = $rawKey;

						// Remove the processed category from the raw categories
						unset($this->rawCategories[$rawKey]);

						// Increment the count for the loop
						$count++;
					}
				}

				// Set level total
				$total = count($counter);
				foreach ($counter as $key => $val) {
					$newSort[$key]['level_total_results'] = $total;
				}
			}

			// Set the new sort to the sorted categories array
			$this->sortedCategories = $newSort;
		}

		// If max depth has been set and reached, we need to end the loop
		if ($this->maxDepth and $level === $this->maxDepth) {
			return true;
		}

		// If $return never got set false, it’s time to end the recursive loop
		if ($return) {
			return true;
		}

		// Go into recursive loop
		$this->_sortAndNest($level);

		// And now return
		return true;
	}

	/**
	 * Set URL Path Variable
	 *
	 * @access      private
	 * @return      bool
	 */
	private function _setUrlPathVar()
	{
		$previousLevel = 1;
		$parents = array();

		foreach ($this->sortedCategories as $key => $val) {
			if ($val['cat_level'] == 1) {
				$previousLevel = 1;
				$parents = array();

				$this->sortedCategories[$key]['cat_url_path'] = $val['cat_url_title'];
			} else {
				if ($previousLevel > $val['cat_level']) {
					$sliceNum = $previousLevel - $val['cat_level'];

					$parents = array_slice($parents, false, -$sliceNum);
				}

				$previousLevel = (int) $val['cat_level'];

				if (! in_array($val['parent_id'], $parents)) {
					$parents[] = $val['parent_id'];
				}

				$path = '';

				foreach ($parents as $pKey => $pVal) {
					$path .= $this->sortedCategories[$pVal]['cat_url_title'] . '/';
				}

				$path .= $val['cat_url_title'];

				$this->sortedCategories[$key]['cat_url_path'] = $path;

				$this->sortedCategories[$val['parent_id']]['has_children'] = true;
			}
		}

		// Set categories with no children has_children variable to false
		foreach ($this->sortedCategories as $key => $val) {
			if (! isset($val['has_children'])) {
				$this->sortedCategories[$key]['has_children'] = false;
			}
		}

		return true;
	}

	/**
	 * Insert Category Into Database
	 *
	 * @access      public
	 * @param       array
	 * @param       bool
	 * @return      mixed
	 */
	public function insertCategory($data = array(), $returnCatInfo = false)
	{
		$defaultData = array(
			'group_id' => false,
			'parent_id' => '0',
			'cat_name' => false,
			'cat_url_title' => false,
			'cat_description' => false,
			'cat_order' => false
		);

		$data = array_merge($defaultData, $data);

		if (
			! $data['group_id'] or
			! $data['cat_name'] or
			! $data['cat_url_title']
		) {
			return false;
		}

		if (! $data['cat_order']) {
			// Get previous order
			$prevOrder = $this->getCategories(array(
				'groupId' => $data['group_id'],
				'parentId' => $data['parent_id'],
				'orderBy' => 'cat_order',
				'sort' => 'desc',
				'singleRow' => true
			));

			$data['cat_order'] = $prevOrder ? $prevOrder['cat_order'] + 1 : 1;
		}

		ee()->db->insert('categories', $data);

		$categoryInfo = $this->getCategories(array(
			'groupId' => $data['group_id'],
			'parentId' => $data['parent_id'],
			'catName' => $data['cat_name'],
			'catSlug' => $data['cat_url_title'],
			'singleRow' => true,
		));

		$catFieldData = array(
			'cat_id' => $categoryInfo['cat_id'],
			'group_id' => $categoryInfo['group_id']
		);

		ee()->db->insert('category_field_data', $catFieldData);

		if (ee()->extensions->active_hook('category_save')) {
			ee()->extensions->call(
				'category_save',
				$categoryInfo['cat_id'],
				array()
			);
		}

		if ($returnCatInfo) {
			return $categoryInfo;
		} else {
			return true;
		}
	}

	/**
	 * Update Categories
	 *
	 * @access      public
	 * @param       array
	 * @return      bool
	 */
	public function updateCats($saveData = array())
	{
		foreach ($saveData as $key => $val) {
			if (! isset($val['cat_id'])) {
				return false;
			}
		}

		ee()->db->update_batch('categories', $saveData, 'cat_id');

		if (ee()->extensions->active_hook('category_save')) {
			ee()->extensions->call('category_save', $val['cat_id'], array());
		}

		return true;
	}

	/**
	 * Delete Categories
	 *
	 * @access      public
	 * @param       string
	 * @return      bool
	 */
	public function deleteCats($catId)
	{
		if (! $catId) {
			return false;
		}

		$this->catDeleteArray = array(
			$catId
		);

		$this->_buildDeleteArray();

		if (ee()->extensions->active_hook('category_delete')) {
			ee()->extensions->call('category_delete', $this->catDeleteArray);
		}

		ee()->db->where_in('cat_id', $this->catDeleteArray);
		ee()->db->delete('categories');

		ee()->db->where_in('cat_id', $this->catDeleteArray);
		ee()->db->delete('category_field_data');

		ee()->db->where_in('cat_id', $this->catDeleteArray);
		ee()->db->delete('category_posts');

		ee()->db->where_in('cat_id', $this->catDeleteArray);
		ee()->db->delete('file_categories');

		return true;
	}

	/**
	 * Build array of categories to delete
	 *
	 * @access      private
	 * @return      bool
	 */
	private function _buildDeleteArray()
	{
		$proceed = true;

		$query = ee()->db
			->select('cat_id')
			->from('categories')
			->where_in('parent_id', $this->catDeleteArray)
			->get();

		$result = $query->result_array();

		foreach ($result as $key => $val) {
			if (! in_array($val['cat_id'], $this->catDeleteArray)) {
				$proceed = false;

				$this->catDeleteArray[] = $val['cat_id'];
			}
		}

		if ($proceed) {
			return true;
		}

		$this->_buildDeleteArray();

		return true;
	}
}