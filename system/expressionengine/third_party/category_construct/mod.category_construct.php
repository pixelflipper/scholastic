<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

// Include configuration
include(PATH_THIRD . 'category_construct/config.php');

/**
 * Category Construct Class
 *
 * @package    category_construct
 * @author     TJ Draper <tj@buzzingpixel.com>
 * @link       https://buzzingpixel.com/ee-add-ons/category-construct
 * @copyright  Copyright (c) 2015, BuzzingPixel
 */
class Category_construct {

	/*------------------------------------*\
		# Public Methods
	\*------------------------------------*/

	/**
	 * Constructor
	 *
	 * @access      public
	 */
	public function __construct()
	{
		// Load the model
		ee()->load->model('category_construct_model');

		// Set default class vars
		$this->namespace = 'construct';
		$this->tagData = '';
	}

	/**
	 * Categories Tag
	 *
	 * @access     public
	 * @return     string
	 */
	public function categories()
	{
		// Get tag params
		$groupId = ee()->TMPL->fetch_param('group_id');
		$catId = ee()->TMPL->fetch_param('cat_id');
		$entryId = ee()->TMPL->fetch_param('entry_id');
		$showEmpty = ee()->TMPL->fetch_param('show_empty') !== 'false';
		$directParent = ee()->TMPL->fetch_param('direct_parent');
		$catSlug = ee()->TMPL->fetch_param('cat_url_title');
		$namespace = ee()->TMPL->fetch_param('namespace');
		$maxDepth = (int) ee()->TMPL->fetch_param('max_depth');
		$this->parentId = ee()->TMPL->fetch_param('parent_id');
		$this->nested = ee()->TMPL->fetch_param('nested') !== 'false';
		$this->initialTagData = ee()->TMPL->tagdata;
		$customFields = ee()->TMPL->fetch_param('custom_fields') === 'true' ? true : false;

		// Set namespacing if applicable
		if ($namespace) {
			$this->namespace = $namespace;
		}

		// Validate that there is a group id
		if (! $groupId and ! $catId and ! $entryId) {
			return false;
		}

		// Get applicable categories
		$this->cats = ee()->category_construct_model->getCategories(array(
			'catId' => $catId,
			'groupId' => $groupId,
			'entryId' => $entryId,
			'showEmpty' => $showEmpty,
			'maxDepth' => $maxDepth,
			'parentId' => $directParent,
			'catSlug' => $catSlug,
			'customFields' => $customFields
		));

		// If any of the following are set, we need to treat all as level 1
		if ($catId or $catSlug or ! $this->nested) {
			foreach ($this->cats as $key => $val) {
				$this->cats[$key]['cat_level'] = 1;
			}
		}

		// If a parent ID is set, we have some work to do
		if ($this->parentId) {
			// Find out how deep the parent of the category we want is
			$parentLevel = $this->cats[$this->parentId]['cat_level'];

			// Set a var for the new nodes
			$this->newCats = array();

			// Set a var for ids we need to search for
			$this->newCatIds = array(
				$this->parentId
			);

			// Go to a function to prune out what we don't need
			$this->_pruneToParent();

			// Reset the categories based on the new categories we just pruned
			$this->cats = $this->newCats;

			// Reset all the node levels
			foreach ($this->cats as $key => $val) {
				$this->cats[$key]['cat_level'] = $val['cat_level'] - $parentLevel;
			}
		}

		$this->_parseCategoryTags();

		return $this->tagData;
	}





	/*------------------------------------*\
		# Private functions
	\*------------------------------------*/

	/**
	 * Prune Categories to Parent
	 *
	 * @access     private
	 * @return     void
	 */
	private function _pruneToParent()
	{
		// Set this itteration var to an array
		$itterationCats = array();

		// Loop through all the categories
		foreach ($this->cats as $key => $val) {
			// If the cats' parent ID is in the newNodeIds array, we want it
			if (in_array($val['parent_id'], $this->newCatIds)) {
				$itterationCats[$key] = $val;

				$this->newCatIds[] = $val['cat_id'];

				// Now we need to unset since we're done with it
				unset($this->cats[$key]);
			}
		}

		// If we didn't get any categories in this itteration, we are done
		if (empty($itterationCats)) {
			return;
		}

		// The categories we picked up on this itteration need
		// to be set to the new cats var
		$this->newCats = array_merge($this->newCats, $itterationCats);

		// We need to check recursively
		$this->_pruneToParent();
	}

	/**
	 * Parse Categories Tag Pair
	 *
	 * @access     private
	 * @return     void
	 */
	private function _parseCategoryTags($level = 1)
	{
		// Set level vars
		$thisLevelParsed = array();

		// Loop through cats and parse the cats applicable for this level
		foreach ($this->cats as $key => $cat) {
			if ($cat['cat_level'] == $level) {
				// Set vars for this node
				$catId = $cat['cat_id'];
				$catParent = $cat['parent_id'];

				// Namespace the node vars
				$cat = $this->_namespaceCat($cat);

				// Parse the node through the EE template parser
				$thisData = ee()->TMPL->parse_variables(
					$this->initialTagData,
					array(
						0 => $cat
					)
				);

				// Replace this category's children tag
				$thisData = str_replace(
					'{' . $this->namespace .':children}',
					'{children_of_parent_' . $catId . '}',
					$thisData
				);

				// Set this category to the level parsed variable
				$thisLevelParsed[$catId]['parent_id'] = $catParent;
				$thisLevelParsed[$catId]['data'] = $thisData;
			}
		}

		// If the level parsed var is empty, it's time to end recursive loop
		if (empty($thisLevelParsed)) {
			// Get rid of the remaining children tags
			$this->tagData = preg_replace('/{children_of_parent_.*}/', '', $this->tagData);

			// Return control to the parent
			return;
		}

		// If level is 1, just append the data. Else loop through and put it
		// in the appropriate places
		if ($level === 1) {
			foreach ($thisLevelParsed as $val) {
				$this->tagData .= $val['data'];
			}
		} else {
			foreach ($thisLevelParsed as $val) {
				$this->tagData = str_replace(
					'{children_of_parent_' . $val['parent_id'] . '}',
					$val['data'] . '{children_of_parent_' . $val['parent_id'] . '}',
					$this->tagData
				);
			}
		}

		// Enter into recursive loop
		$this->_parseCategoryTags($level + 1);

		return;
	}

	/**
	 * Namespace Variables
	 *
	 * @access     private
	 * @return     array
	 */
	private function _namespaceCat($cat)
	{
		// Set the return var
		$returnCats = array();

		// Loop through each variable in the node and namespace it
		foreach ($cat as $var => $val) {
			$var = $this->namespace . ':' . $var;

			$returnCats[$var] = $val;
		}

		// Return the nodes
		return $returnCats;
	}
}