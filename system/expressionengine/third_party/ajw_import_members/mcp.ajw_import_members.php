<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * AJW Simple Module Control Panel File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Andrew Weaver
 * @link		http://brandnewbox.co.uk/
 */

class Ajw_import_members_mcp {
	
	public $return_data;
	
	private $module_name = "ajw_import_members";
	private $_base_url;
	private $_form_url;
	private $_theme_url;	
	
	var $settings;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->EE =& get_instance();

		// Determine some common URLs
		$this->_base_url = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->_form_url = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->_theme_url = $this->EE->config->item('theme_folder_url') . 'third_party/'.$this->module_name.'/';

		// Load stylesheet from the themes folder
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'. $this->_theme_url .'css/'.$this->module_name.'.css" />');
		
		// Create navigation menu
		/*
		$this->EE->cp->set_right_nav(
			array (
				'ajw_import_members_index' => $this->_base_url,
				'ajw_import_members_edit' => $this->_base_url.AMP.'method=edit_item'
			)
		);
		*/
		
		// Load model
		$this->EE->load->model('ajw_import_members_model', 'import');
		
	}
	
	/*
		CONTROLLER functions
	*/
	
	/**
	 * Default controller. Displays a list of items
	 *
	 * @return 	void
	 */
	public function index() {
		
		// Set page title and breadcrumb
		$this->EE->view->cp_page_title = lang('ajw_import_members_index');
		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));
		
		// Load libraries and helpers
		$this->EE->load->library('pagination');
		$this->EE->load->library('javascript');
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();

		$data["action_url"] = $this->EE->functions->fetch_site_index(0, 0) . QUERY_MARKER . 'ACT=' . $this->EE->cp->fetch_action_id('Ajw_import_members', 'run_action') . AMP . "id=";

		$this->EE->db->select('id, name, description, passkey');
		$this->EE->db->order_by('id ASC');
		$query = $this->EE->db->get('exp_ajw_import_members');
		$data["saved_imports"] = array();
		foreach($query->result_array() as $row) {
			$id = $row["id"];
			$row["name"] = '<a href="'.$this->_base_url.AMP.'method=save'.AMP.'id='.$row["id"].'">' . $row["name"] . '</a>';
			$row[] = '<a href="'.$this->_base_url.AMP.'method=load'.AMP.'id='.$row["id"].'">Configure</a>';
			$row[] = '<a href="'.$this->_base_url.AMP.'method=run'.AMP.'id='.$row["id"].'">Run import</a>';
			$row[] = '<a class="passkey" href="'.$data["action_url"].$id.( $row["passkey"] != '' ? AMP.'passkey='.$row["passkey"] : '' ).'">Import URL</a>';
			$row[] = '<a href="'.$this->_base_url.AMP.'method=delete'.AMP.'id='.$row["id"].'">Delete</a>';
			unset( $row["passkey"] );
			$data["saved_imports"][ $id ] = $row;
		}
		
		// Add javascript for toggle checkbox
		$this->EE->javascript->output(
			array(
				'
				$(".toggle_all").toggle(
					function(){
						$("input.toggle").each(function() {
							this.checked = true;
						});
					}, function (){
						var checked_status = this.checked;
						$("input.toggle").each(function() {
							this.checked = false;
						});
					}
				);
				'
			)
		); 
		$this->EE->javascript->compile();

		// todo: include sort by column javascript?

		// Set up stuff for a new import
		$data["types"] = $this->EE->import->fetch_datatype_names();
		$data["form_action"] = $this->_form_url . AMP . "method=settings";

		// Set name of view
		$data["view"] = 'index';
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}

	function settings() {

		// Handle form input
		$this->_get_input();

		// Set page title and breadcrumb
		$this->EE->view->cp_page_title = lang('ajw_import_members_settings');
		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));
		
		// Load libraries and helpers
		$this->EE->load->library('javascript');
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();

		// Get settings form for type
		$this->EE->import->initialise_types();
		$data["settings"] = $this->EE->import->datatypes[ 
			$this->settings["import"]["type"] ]->settings_form( $this->settings );

		// Form action URL
		$data["form_action"] = $this->_form_url . AMP . "method=configure";

		// Set name of view
		$data["view"] = 'settings';
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}

	function configure() {

		// Handle form input
		$this->_get_input();

		// Set page title and breadcrumb
				$this->EE->view->cp_page_title = lang('ajw_import_members_configure');
		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));
		
		// Load libraries and helpers
		$this->EE->load->library('javascript');
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();

		// Get standard member fields
		$base_fields = array(
			// 'bday_y', 'bday_m', 'bday_d', 
			'url', 
			'location', 'occupation', 'interests', 'aol_im',
			'icq', 'yahoo_im', 'msn_im', 'bio');
			
		$data["standard_fields"] = array();
		$this->EE->lang->loadfile('myaccount');
		foreach( $base_fields as $field ) {
			$data["standard_fields"][ $field ] = lang( $field );
		}

		// Get custom member fields
		$this->EE->db->select( "m_field_id, m_field_label" );
		$this->EE->db->order_by( "m_field_order" );
		$query = $this->EE->db->get( "exp_member_fields" );
		$data["custom_member_fields"] = array();
		foreach( $query->result_array() as $row ) {
			$data["custom_member_fields"][ $row["m_field_id"] ] = $row["m_field_label"];
		}

		// Get list of fields from the datatype
				
		$this->EE->import->initialise_types();
		$this->EE->import->datatypes[ $this->settings["import"]["type"] ]->initialise( $this->settings );
		$ret = $this->EE->import->datatypes[ $this->settings["import"]["type"] ]->fetch();
		$data["data_fields"][""] = "";
		if( $ret != -1 ) {
			$fields = $this->EE->import->datatypes[ $this->settings["import"]["type"] ]->fetch_columns();
			foreach( $fields as $key => $value ) {
				$data["data_fields"][ $key ] = $value;
			}
		} else {
			$data["errors"] = $this->EE->import->datatypes[ $this->settings["import"]["type"] ]->errors;	
		}
		
		// Get member groups
		
		$this->EE->db->select( "group_id, group_title" );
		$this->EE->db->where( "site_id", $this->EE->config->item('site_id') );
		$this->EE->db->order_by( "group_id" );
		$query = $this->EE->db->get( "exp_member_groups" );
		$data["member_groups"] = array();
		foreach( $query->result_array() as $row ) {
			$data["member_groups"][ $row["group_id"] ] = $row["group_title"];
		}
		
		// Form action URL
		$data["form_action"] = $this->_form_url . AMP . "method=import";
		$data["save_action"] = $this->_form_url . AMP . "method=save";
		$data["back_link"] = $this->_base_url . AMP . "method=settings";

		// Set name of view
		$data["view"] = 'configure';
		
		$data['default_settings'] = $this->settings;
		
		if( $this->EE->input->get( "id" ) ) {
			$data["id"] = $this->EE->input->get( "id" );
		}
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}

	function import() {
		
		if( $this->EE->input->post("save") !== FALSE ) {
			return $this->save();
			exit;
		}

		// Handle form input
		$this->_get_input();
		// print_r( $this->settings ); exit;

		// Set page title and breadcrumb
		$this->EE->view->cp_page_title = lang('ajw_import_members_import');
		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));
		
		// Load libraries and helpers
		$this->EE->load->library('javascript');
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();

		// Get settings form for type
		$this->EE->import->initialise_types();
		$data["results"] = $this->EE->import->do_import( 
			$this->EE->import->datatypes[ $this->settings["import"]["type"] ], 
			$this->settings 
			);

		// Form action URL
		if( isset( $this->settings["import"]["id"] ) ) {
			$data["id"] = $this->settings["import"]["id"];
		} else {
			$data["id"] = 0;
		}

		// Form action URL
		$data["form_action"] = $this->_form_url . AMP . "method=save";

		// Set name of view
		$data["view"] = 'import';
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
		
	}

	function save() {
		
		$id = $this->EE->input->get_post("id", 0);
		
		$this->_get_input();
		
		// Load helpers
		$this->EE->load->library('table');
		$this->EE->load->helper('form');

		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));

		// Set data
		if ( $id == 0 ) {
			
			$this->EE->view->cp_page_title = lang('ajw_import_members_save');
			$name = "";
			$description = "";
			$passkey = "";

		} else {

			$this->EE->view->cp_page_title = lang('ajw_import_members_update');
			
			$this->EE->db->where('id', $id );
			$query = $this->EE->db->get('exp_ajw_import_members');
			$row = $query->row_array();
			
			$name = $row["name"];
			$description = $row["description"];
			$passkey = $row["passkey"];
			
		}
		
		$data["content"] = 'save';
		
		$data["form"] = array(
			array( 
				'<em class="required">*</em> ' .
				form_label('Name', 'name') .
				'<div class="subtext">A title for the import</div>',  
				form_input(
					array(
						'name' => 'name',
						'id' => 'name',
						'value' => $name,
						'size' => '50'
						)
					) 
				),
			array( 
				form_label('Description', 'description') .
				'<div class="subtext">A description of the import</div>', 
				form_textarea(
					array(
						'name' => 'description',
						'id' => 'description',
						'value' => $description,
						'rows' => '4',
						'cols' => '64'
						)
					)
				),
			array( 
				form_label('Passkey', 'passkey') . 
				'<div class="subtext">Add a passkey to increase security against<br/>saved imports being run inadvertently</div>', 
				form_input(
					array(
						'name' => 'passkey',
						'id' => 'passkey',
						'value' => $passkey,
						'style' => 'width:50%'
						)
					) . NBS . 
					form_button(
						array(
							'id' => 'generate', 
							'name' => 'generate',
							'content' => 'Generate random key'
						) 
					)
				) 
		);

		$data["id"] = $id;
		
		// Form action URL
		$data["form_action"] = $this->_form_url.AMP.'method=do_save';
		
		$this->EE->load->library('javascript');
		$this->EE->javascript->output('
			var chars = "0123456789ABCDEF";
			var string_length = 32;
		$("#generate").click( function() {
			var randomstring = "";
			for (var i=0; i<string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
			}
			$("#passkey").val(randomstring);
		});
		');
		$this->EE->javascript->compile();
		
		$data["view"] = 'save';	
		
		// Load view
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}

	function do_save() {

		$this->_get_input();

		$this->EE->load->helper('date');

		$id = $this->EE->input->post("id");

		$data = array(
			'name' => $this->EE->input->post( "name" ),
			'description' => $this->EE->input->post( "description" ),
			'passkey' => $this->EE->input->post( "passkey" ),
			'last_run' => now()
		);
		
		if( isset( $this->settings["import"]["type"] ) ) {
			$data['settings'] = serialize( $this->settings );
		} else {
			// Fetch settings from database
			$this->EE->db->select('settings');
			$this->EE->db->where('id', $id);
			$query = $this->EE->db->get('exp_ajw_import_members');
			$row = $query->row_array();
			$data['settings'] = $row["settings"];
			$this->settings = unserialize( $data['settings'] );
		}
		
		if( $id == "" OR $id == "0" ) {
			$this->EE->db->insert('exp_ajw_import_members', $data);
		} else {
			$this->EE->db->where('id', $id );
			$this->EE->db->update('exp_ajw_import_members', $data);	
		}

		$this->EE->session->set_flashdata('message_success', "Import saved.");

		$this->EE->functions->redirect($this->_base_url.AMP."method=index"); 
		
	}

	function load() {

		if ( $this->EE->input->get( "id" ) != 0 ) {
			$this->EE->db->where('id', $this->EE->input->get( "id" ) );
			$query = $this->EE->db->get('exp_ajw_import_members');
			$row = $query->row_array();
			$this->settings = unserialize($row["settings"]);
			$this->settings["import"]["id"] = $this->EE->input->get( "id" );
			$this->_set_session( 'settings', serialize( $this->settings ) );
		}

		$this->EE->functions->redirect($this->_base_url.AMP."method=configure".AMP."id=".$this->EE->input->get( "id" )); 
	}

	function run() {

		if ( $this->EE->input->get( "id" ) != 0 ) {
			$this->EE->db->where('id', $this->EE->input->get( "id" ) );
			$query = $this->EE->db->get('exp_ajw_import_members');
			$row = $query->row_array();
			$this->settings = unserialize($row["settings"]);
			$this->settings["import"]["id"] = $this->EE->input->get( "id" );
			$this->_set_session( 'settings', serialize( $this->settings ) );
		}

		$this->EE->functions->redirect($this->_base_url.AMP."method=import"); 
	}

	function delete() {
		
		$id = $this->EE->input->get( "id" );

		// Set page title
		$this->EE->view->cp_page_title = lang('ajw_import_members_confirm_delete');
		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));

		// Load helpers
		$this->EE->load->helper('form');

		// Set data
		$data["title"] = "Confirm delete";
		$data["content"] = 'delete';
		
		$data["id"] = $id;
		
		// Form action URL
		$data["form_action"] = $this->_form_url.AMP.'method=do_delete';
		
		// Load view
		return $this->EE->load->view('_wrapper', $data, TRUE);
		
	}

	function do_delete() {
		
		$id = $this->EE->input->post("id");

		if( $id != "" && $id != "0" ) {
			$this->EE->db->where('id', $id );
			$this->EE->db->delete('exp_ajw_import_members');	
		}
		
		$this->EE->session->set_flashdata('message_success', "Deleted");

		$this->EE->functions->redirect($this->_base_url.AMP."method=index");
		
	}

	/**
	 * Edit form to create or edit an item
	 *
	 * @param string $id 
	 * @return void
	 * @author Andrew Weaver
	 */
	function edit_item( $id=NULL ) {

		// Set page title and breadcrumb
		$this->EE->view->cp_page_title = lang('ajw_import_members_edit');
		$this->EE->cp->set_breadcrumb($this->_base_url, lang('ajw_import_members_module_name'));
		
		// Load libraries and helpers
		$this->EE->load->library('pagination');
		$this->EE->load->library('javascript');
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();

		// Load form data from model
		$data["form"] = $this->EE->import->edit_item_form();

		// If editing an existing entry, load the data from the model
		if( $id == NULL ) {
			// See if the id is being passed through the URL
			$id = $this->EE->input->get("id", FALSE);
		}
		if( $id !== FALSE ) {
			// Fetch data from model
			$data["settings"] = $this->EE->import->fetch( $id );
		}

		// Set up form actions
		$data["update_item_action"] = $this->_form_url . AMP . "method=update_item";

		// Set name of view
		$data["view"] = 'edit_item';
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}

	/*
		FORM ACTIONS
	*/

	/**
	 * Function to handle bulk form edits, deletes, etc.
	 *
	 * @return void
	 * @author Andrew Weaver
	 */
	function bulk_edit() {

		$data = array();

		if( $this->EE->input->post('confirm') != 'confirm' ) {

			// Change this depending on required action

			$data["confirm_message"] = lang('ajw_import_members_confirm');
			$data["toggle"] = $this->EE->input->post('toggle');
			$data["action"] = $this->EE->input->post('action');
			$data["bulk_edit_action"] = $this->_form_url . AMP . "method=bulk_edit";

			if( $data["action"] == "edit" ) {
				$data["confirm_message"] = lang('ajw_import_members_confirm');
			}

			$data["view"] = "bulk_edit";

			return $this->EE->load->view('_wrapper', $data, TRUE);

		} else {

			// Set message based on action
			$message = lang('ajw_import_members_successful');

			// Perform action on toggle[] items then return
			if( $this->EE->input->post('action') == "edit" ) {
				$ids = $this->EE->input->post('toggle');
				// Do something with $ids

				// Set message based on action
				$message = lang('ajw_import_members_successful');
			}
			
			$this->EE->session->set_flashdata('message_success', $message);
			$this->EE->functions->redirect($this->_base_url);
			
		}
		
	}
	
	/**
	 * Validates, then adds or updates an item
	 *
	 * @return void
	 * @author Andrew Weaver
	 */
	function update_item() {

		// Load form data from model
		$form = $this->EE->import->edit_item_form();
		
		$this->EE->load->library('form_validation');
		$this->EE->form_validation->set_error_delimiters('<p class="notice">', '</p>');
		
		foreach( $form as $form_name => $form_data ) {
			if( isset( $form_data["required"] ) ) {
				$this->EE->form_validation->set_rules($form_name, lang($form_name), $form_data["required"]);
			}
		}
		
		if ($this->EE->form_validation->run() === FALSE) {
			// Validate failed
			return $this->edit_item( $this->EE->input->post("id") );
		}
		
		// Validation passed
		
		// Load array with data from form
		$data = array(
			"title" => $this->EE->input->post("title")
		);
		
		// Determine whether to insert or update table
		if( $this->EE->input->post("id") !== FALSE ) {
			$this->EE->db->where('id', $this->EE->input->post("id") );
			// $this->EE->db->update('exp_...', $data);
			$this->EE->session->set_flashdata( 'message_success', lang('ajw_import_members_updated') );
		} else {
			// $this->EE->db->insert('exp_...', $data);
			$this->EE->session->set_flashdata( 'message_success', lang('ajw_import_members_added') );
		}
		
		$this->EE->functions->redirect( $this->_base_url ); 
	}

	/*
		HELPER functions
	*/


	/**
	 * Add $data to user session
	 *
	 * @param string $key 
	 * @param string $data 
	 * @return void
	 */
	function _set_session( $key, $data ) {
		@session_start();
		if ( !isset( $_SESSION[ $this->module_name ] ) ) {
			$_SESSION[ $this->module_name ] = array();
		}
		$_SESSION[ $this->module_name ][ $key ] = $data;
	}

	/**
	 * Retrieve data from session. Data is removed from session unless $keep is
	 * set to TRUE
	 *
	 * @param string $key 
	 * @param string $keep 
	 * @return void $data
	 */
	function _get_session( $key, $keep = FALSE ) {
		@session_start();  
		if( isset( $_SESSION[ $this->module_name ] ) ) {
			if( isset( $_SESSION[ $this->module_name ][ $key ] ) ) {
				$data = $_SESSION[ $this->module_name ][ $key ];
				if ( $keep != TRUE ) {
		    	unset($_SESSION[ $this->module_name ][ $key ]); 
		    	unset($_SESSION[ $this->module_name ]); 
				}
				return( $data );
			}
		}
		return "";
	}

	/**
	 * Handle input from forms, sessions
	 * 
	 * Collects data from forms, query strings and sessions. Only keeps relevant data
	 * for the current import data type. Stores in session to allow back-and-forth
	 * through 'wizard'
	 *
	 */
	function _get_input() {
	
		// Get current settings from session
		$this->settings = unserialize( $this->_get_session( 'settings' ) );

		$import_step = $this->EE->input->get_post("step", "default");
		switch( $import_step ) {

			// Step 1: choose import type
			case "index": {
				$this->settings["import"]["type"] = $this->EE->input->get_post("type");
				break;
			}

			// Step 2: set up datatype
			case "settings": {
				// Check datatype specific settings
				if( isset( $this->settings["import"]["type"] ) && 
					$this->settings["import"]["type"] != "" ) {
					$this->EE->import->initialise_types();
					$datatype_settings = $this->EE->import->datatypes[ 
						$this->settings["import"]["type"] ]->settings;
					foreach( $datatype_settings as $option => $value ) {
						if( $this->EE->input->get_post( $option ) !== FALSE ) {
							$this->settings["datatype"][ $option ] = 
								$this->EE->input->get_post( $option );
						}
					}
				}
				break;
			}
		
			case "configure": {
				
				foreach( $_POST as $key => $setting ) {
					if( $this->EE->input->post( $key ) !== FALSE ) {
						$this->settings["config"][ $key ] = $this->EE->input->post( $key );
					}
				}
				
				break;
			}		

			default: {
			}

		}

		// Get saved import id
		if( $this->EE->input->get( "id" ) !== FALSE ) {
			$this->settings["import"][ "id" ] = $this->EE->input->get_post( "id" );
		}

		// print_r( $this->settings ); exit;

		// Store settings in session
		$this->_set_session( 'settings', serialize( $this->settings ) );
	}

	function clear() {
		$this->_set_session( 'settings', serialize( array() ) );
	}

}
/* End of file mcp.ajw_import_members.php */
/* Location: /system/expressionengine/third_party/ajw_import_members/mcp.ajw_import_members.php */