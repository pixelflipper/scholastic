<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * AJW Simple Module Front End File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		Andrew Weaver
 * @link		http://brandnewbox.co.uk/
 */

class Ajw_import_members {
	
	public $return_data;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->EE =& get_instance();
	}
	
	/**
	 * Run an import via an action
	 *
	 * @return void
	 * @author Andrew Weaver
	 */
	function run_action() {
		
		$this->EE->load->model('ajw_import_members_model', 'import');
		
		$id = "";
		if( $this->EE->input->get("id") != "" ) {
			$id = $this->EE->input->get("id");
		} 

		if( $id == "" ) {
			exit;
		}

		$this->EE->load->helper('url');
		$this->EE->load->library('javascript'); 
		$this->EE->load->model('template_model'); 
		$this->EE->load->helper('security');

		// Fetch import settings
		$this->EE->db->where('id', $id );
		$query = $this->EE->db->get('exp_ajw_import_members');
		if( $query->num_rows() == 0 ) {
			exit;
		}
		$row = $query->row_array();
		$this->settings = unserialize($row["settings"]);

		if( $row["passkey"] != "" ) {
			if( $row["passkey"] != $this->EE->input->get("passkey") ) {
				exit;
			}
		}

		// Initialise
		$this->EE->import->initialise_types();

		// Check for modifiers
		if( $this->EE->input->get('filename') !== FALSE ) {
			if( $this->EE->input->get('filename') == "POST" ) {
				if( $this->EE->input->post('data') !== FALSE ) {
					$data = $this->EE->input->post('data');
					// write to cache file
					// set filename to cache file
					// clean up cache
					print tempnam("/tmp", ''); exit;
				}
			}
			$this->settings["datatype"]["filename"] = $this->EE->input->get('filename');
		}

		// Do import
		$this->return_data .= $this->EE->import->do_import( 
			$this->EE->import->datatypes[ $this->settings["import"]["type"] ], 
			$this->settings 
			);

		$this->return_data .= "<p>Import has finished.</p>";
		
		print $this->return_data;
		exit;
	}
	
}
/* End of file mod.ajw_import_members.php */
/* Location: /system/expressionengine/third_party/ajw_import_members/mod.ajw_import_members.php */