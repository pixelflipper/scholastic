<?php

    $this->EE =& get_instance();
	$this->EE->cp->load_package_css('settings');
	$this->EE->cp->load_package_js('settings');
?>
	<p class="escort-intro"><?php echo $this->EE->lang->line('escort_intro'); ?></p>

<?php
	echo form_open('C=addons_extensions'.AMP.'M=save_extension_settings', array('id' => $file), array('file' => $file));
?>

<?php foreach($services as $service => $settings) : ?>

<table class="mainTable padTable <?php echo $service; ?>" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th colspan="2"><?php echo $this->EE->lang->line('escort_'.$service.'_name'); ?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2" class="escort-service-description">
				<?php echo $this->EE->lang->line('escort_'.$service.'_description'); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $this->EE->lang->line('escort_enable').' '.$this->EE->lang->line('escort_'.$service.'_name').'?'; ?></td>
			<td>
				<label class="escort-checkbox"><?php echo form_checkbox($service.'_active', 'y', (!empty($current[$service.'_active']) && $current[$service.'_active'] == 'y') ? true : false).NBS.$this->EE->lang->line('yes'); ?></label>
				<input type="hidden" name="service_order[]" value="<?php echo $service; ?>" />
			</td>
		</tr>
		<?php foreach($settings as $setting => $type) : ?>
		<tr>
			<td style="width:40%;"><?php echo $this->EE->lang->line('escort_'.$setting); ?></td>
			<td>
				<?php if($type == 'text')
				{
					echo form_input($setting, (!empty($current[$setting])) ? $current[$setting] : ''); 
				}
				?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php endforeach; ?>
	
<?php if(!empty($config_settings)) : ?>
	<p class="escort-warning"><?php echo $this->EE->lang->line('escort_config_warning'); ?></p>
<?php endif; ?>
	
<?php
	echo form_submit(
		array(
			'name' => 'submit',
			'value' => $this->EE->lang->line('escort_save_settings'),
			'class' => 'submit'
		)
	);
	echo form_close();
?>