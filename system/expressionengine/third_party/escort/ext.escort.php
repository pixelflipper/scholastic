<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
    This file is part of Escort add-on for ExpressionEngine.

    Escort is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Escort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    Read the terms of the GNU General Public License
    at <http://www.gnu.org/licenses/>.
    
    Copyright 2013 Derek Hogue - http://amphibian.info
*/

class Escort_ext {
	
	
	var $name = 'Escort';
	var $description = 'Let your transactional email get chaperoned by the pros.';
	var $version = '1.1.2';
	var $docs_url = 'http://amphibian.info/software/ee/escort';
	var $settings_exist	= 'y';
	var $settings = array();
	var $site_id = '';
	var $protocol = '';
	var $email_crlf = '\n';
	var $services = array();
	var $email_in = array();
	var $email_out = array();
	var $debug = false;
	

	function __construct($settings = '')
	{
		$this->EE =& get_instance();
		$this->settings = $settings;
        $this->config = $this->EE->config->item('escort_settings');
		$this->site_id = $this->EE->config->item('site_id');
		$this->protocol = $this->EE->config->item('mail_protocol');
		if($this->EE->config->item('email_crlf') != false)
		{
			$this->email_crlf = $this->EE->config->item('email_crlf');
		}
		$this->services = array(
			'mandrill' => array(
				'mandrill_api_key' => 'text',
				'mandrill_subaccount' => 'text'
			),
			'mailgun' => array(
				'mailgun_api_key' => 'text',
				'mailgun_domain' => 'text'
			),
			'postageapp' => array(
				'postageapp_api_key' => 'text'
			),			
			'postmark' => array(
				'postmark_api_key' => 'text'
			),
			'sendgrid' => array(
				'sendgrid_username' => 'text',
				'sendgrid_password' => 'text'
			)
		);
	}
	

	function settings_form($current)
	{	    
		$current = $this->get_settings();
		$services_sorted = array();
		
		// Look at custom service order
		foreach($current['service_order'] as $service)
		{
			$services_sorted[$service] = $this->services[$service];
		}
		
		// Add any services were not included in the custom order
		foreach($this->services as $service => $settings)
		{
			if(empty($services_sorted[$service]))
			{
				$services_sorted[$service] = $settings;
			}
		}
		
		$vars = array(
			'current' => $current,
			'file' => 'escort',
			'services' => $services_sorted
		);
		
		if(!empty($this->config))
		{
			$vars['config_settings'] = 'y';
		}		
		
		return $this->EE->load->view('settings', $vars, TRUE);
	}
	
	
	function save_settings()
	{
		$settings = $this->get_settings(TRUE);
		
		unset(
			$_POST['file'], 
			$_POST['submit']
		);
						
		foreach($_POST as $k => $v)
		{
			$settings[$this->site_id][$k] = $v;
		}
		
		foreach($this->services as $k => $v)
		{
			if(empty($_POST[$k.'_active']) && isset($settings[$this->site_id][$k.'_active']))
			{
				unset($settings[$this->site_id][$k.'_active']);
			}
		}
					
		$this->EE->db->where('class', ucfirst(get_class($this)));
		$this->EE->db->update('extensions', array('settings' => serialize($settings)));
		$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('preferences_updated'));
	}

	
	function get_settings($all_sites = FALSE)
	{
		$get_settings = $this->EE->db->query("SELECT settings 
			FROM exp_extensions 
			WHERE class = '".ucfirst(get_class($this))."' 
			LIMIT 1");
		
		$this->EE->load->helper('string');
		
		if($get_settings->num_rows() == 1)
        {
        	$settings = strip_slashes(unserialize($get_settings->row('settings')));
        	$settings = ($all_sites == TRUE || empty($settings)) ? $settings : $settings[$this->site_id];
        }
        else
        {
        	$settings = array();
        }
                
        // Check for config settings - they will override database settings

        // Set a service order if none is set
        if(empty($settings['service_order']) && empty($this->config[$this->site_id]['service_order']))
        {
	        $settings['service_order'] = array();
	        foreach($this->services as $service => $service_settings)
	        {
		        $settings['service_order'][] = $service;
	        }
		}
        
        // Override each setting from config
        if(!empty($this->config[$this->site_id]))
        {
	        foreach($this->config[$this->site_id] as $k => $v)
	        {
		        $settings[$k] = $v;
	        }
		}
        
        return $settings;
	}


	function email_send($data)
	{	
		$settings = $this->get_settings();
		if(empty($settings['service_order']))
		{
			return false;
		}
		
		$sent = false;
		$this->email_in = $data;
		unset($data);
		
		// Grats to Justin Kimbrell for the alert and code - remove {unwrap} tags
		$this->email_in['finalbody'] = preg_replace('/'.LD.'\/?unwrap'.RD.'/u', '', $this->email_in['finalbody']);
		
		if($this->debug == true)
		{
			$this->_debug($this->email_in, false);
		}
		
		// Set X-Mailer
		$this->email_out['headers']['X-Mailer'] = $this->email_in['headers']['X-Mailer'].' (via Escort '.$this->version.')';
		
		// From (may include a name)
		$this->email_out['from'] = $this->_name_and_email($this->email_in['headers']['From']);	  
		
		// Reply-To (may include a name)
		if(!empty($this->email_in['headers']['Reply-To']))
		{
			$this->email_out['reply-to'] = $this->_name_and_email($this->email_in['headers']['Reply-To']);
		}
		
		// To (email-only)
		$this->email_out['to'] = (is_array($this->email_in['recipients'])) ? $this->email_in['recipients'] : $this->_recipient_array($this->email_in['recipients']);
		
		// Cc (email-only)
		if(!empty($this->email_in['cc_array']))
		{
			$this->email_out['cc'] = $this->email_in['cc_array'];
		}
		elseif(!empty($this->email_in['headers']['Cc']))
		{
			$this->email_out['cc'] = $this->_recipient_array($this->email_in['headers']['Cc']);
		}

		// Bcc (email-only)
		if(!empty($this->email_in['bcc_array']))
		{
			$this->email_out['bcc'] = $this->email_in['bcc_array'];
		}
		elseif(!empty($this->email_in['headers']['Bcc']))
		{
			$this->email_out['bcc'] = $this->_recipient_array($this->email_in['headers']['Bcc']);
		}
		
		// Subject	
		$subject = (!empty($this->email_in['subject'])) ? $this->email_in['subject'] : $this->email_in['headers']['Subject'];
		$this->email_out['subject'] = (strpos($subject, '?Q?') !== false) ? $this->_decode_q($subject) : $subject;
		
		
		// Set HTML/Text and attachments
		$this->_body_and_attachments();
		
		if($this->debug == true)
		{
			$this->_debug($this->email_out);
		}
			
		foreach($settings['service_order'] as $service)
		{
			if(!empty($settings[$service.'_active']) && $settings[$service.'_active'] == 'y')
			{
				switch($service)
				{
					case 'mailgun':
						if(!empty($settings['mailgun_api_key']) && !empty($settings['mailgun_domain']))
						{
							$sent = $this->_send_mailgun($settings['mailgun_api_key'], $settings['mailgun_domain']);
						}
						break;				
					case 'mandrill':
						if(!empty($settings['mandrill_api_key']))
						{
							$subaccount = (!empty($settings['mandrill_subaccount']) ? $settings['mandrill_subaccount'] : '');
							$sent = $this->_send_mandrill($settings['mandrill_api_key'], $subaccount);
						}
						break;
					case 'postageapp':
						if(!empty($settings['postageapp_api_key']))
						{
							$sent = $this->_send_postageapp($settings['postageapp_api_key']);
						}
						break;	
					case 'postmark':
						if(!empty($settings['postmark_api_key']))
						{
							$sent = $this->_send_postmark($settings['postmark_api_key']);
						}
						break;				
					case 'sendgrid':
						if(!empty($settings['sendgrid_username']) && !empty($settings['sendgrid_password']))
						{
							$sent = $this->_send_sendgrid($settings['sendgrid_username'], $settings['sendgrid_password']);
						}
						break;
				}
			}
			
			if($sent == true)
			{
				$this->EE->extensions->end_script = true;
				return true;
			}
		}
		
		return false;
				  
	}
	
	
	/**
		Sending methods for each of our services follow.
	**/

	function _send_mandrill($api_key, $subaccount)
	{
		$content = array(
			'key' => $api_key,
			'message' => $this->email_out
		);
		
		if(!empty($subaccount))
		{
			$content['message']['subaccount'] = $subaccount;
		}
		
		$content['message']['from_email'] = $content['message']['from']['email'];
		if(!empty($content['message']['from']['name']))
		{
			$content['message']['from_name'] = $content['message']['from']['name'];
		}
		unset($content['message']['from']);
		
		$mandrill_to = array();
		
		foreach($content['message']['to'] as $to)
		{
			$mandrill_to[] = array_merge($this->_name_and_email($to), array('type' => 'to'));
		}
		
		if(!empty($content['message']['cc']))
		{
			foreach($content['message']['cc'] as $to)
			{
				$mandrill_to[] = array_merge($this->_name_and_email($to), array('type' => 'cc'));
			}
			unset($content['message']['cc']);
		}
				
		if(!empty($content['message']['reply-to']))
		{
			$content['message']['headers']['Reply-To'] = $this->_recipient_str($content['message']['reply-to'], true);
		}
		unset($content['message']['reply-to']);

		
		if(!empty($content['message']['bcc']))
		{
			foreach($content['message']['bcc'] as $to)
			{
				$mandrill_to[] = array_merge($this->_name_and_email($to), array('type' => 'bcc'));
			}
		}
		unset($content['message']['bcc']);
		
		$content['message']['to'] = $mandrill_to;
						
		$headers = array(
	    	'Accept: application/json',
			'Content-Type: application/json',
		);
		
		if($this->EE->extensions->active_hook('escort_pre_send'))
		{
			$content = $this->EE->extensions->call('escort_pre_send', 'mandrill', $content);
		}
		
		if(!function_exists('json_encode'))
		{
			$this->EE->load->library('Services_json');
		}
		$content = json_encode($content);
				
		return $this->_curl_request('https://mandrillapp.com/api/1.0/messages/send.json', $headers, $content);
	}
	
	
	function _send_mailgun($api_key, $domain)
	{
		$headers = array();
		$email = $this->email_out;
		$email['from'] = $this->_recipient_str($email['from'], true);
	    
	    foreach($email['headers'] as $header => $value)
	    {
		    $email['h:'.$header] = $value;    
	    }
	    unset($email['headers']);
	    
	    if(!empty($email['reply-to']))
	    {
	    	$email['h:Reply-To'] = $this->_recipient_str($email['reply-to'], true);
	    	unset($email['reply-to']);
	    }
	    	    
	    if(!empty($email['attachments']))
	    {
	    	$attachments = $this->_write_attachments();
	    	unset($email['attachments']);
	    	$i = 1;	    	
	    	foreach($attachments as $name => $path)
	    	{
	    		$email['attachment'][$i] = '@'.$path;
	    		$i++;
	    	}
	    }
	    
	    if($this->EE->extensions->active_hook('escort_pre_send'))
		{
			$email = $this->EE->extensions->call('escort_pre_send', 'mailgun', $email);
		}
	    
	    $post = array();
	    $this->_http_build_post($email, $post);
	    
	    return $this->_curl_request("https://api.mailgun.net/v2/$domain/messages", $headers, $post, "api:$api_key");
	}


	function _send_postageapp($api_key)
	{
		$content = array(
			'api_key' => $api_key,
			'uid' => sha1(serialize($this->email_out['to']).$this->email_out['subject'].$this->EE->localize->now),
			'arguments' => array(
				'headers' => array(
					'from' => $this->_recipient_str($this->email_out['from'], true),
					'subject' => $this->email_out['subject'],
				)
			)
		);
		
		foreach($this->email_out['headers'] as $header => $value)
	    {
		    $content['arguments']['headers'][$header] = $value;    
	    }
		
		/*
			All recipients, including Cc and Bcc, must be in the recipients array, and will be Bcc by default.
			Any addresses which are *also* included in the Cc header will be visible as Cc
		*/
		$recipients = $this->email_out['to'];
		if(!empty($this->email_out['cc']))
	    {
	    	$recipients = array_merge($recipients,  $this->email_out['cc']);
	    	$content['arguments']['headers']['cc'] = $this->_recipient_str($this->email_out['cc']);
	    }
	    if(!empty($this->email_out['bcc']))
	    {
	    	$recipients = array_merge($recipients,  $this->email_out['bcc']);
	    }
	    $content['arguments']['recipients'] = $recipients;
		
	    if(!empty($this->email_out['reply-to']))
	    {
	    	$content['arguments']['headers']['reply-to'] = $this->_recipient_str($this->email_out['reply-to'], true);
	    }
	    if(!empty($this->email_out['html']))
	    {
	    	$content['arguments']['content']['text/html'] = $this->email_out['html'];
	    }
	    if(!empty($this->email_out['text']))
	    {
	    	$content['arguments']['content']['text/plain'] = $this->email_out['text'];
	    }
	    if(!empty($this->email_out['attachments']))
	    {
	    	foreach($this->email_out['attachments'] as $attachment)
	    	{
	    		$content['arguments']['attachments'][$attachment['name']] = array(
	    			'content_type' => $attachment['type'],
	    			'content' => $attachment['content']
	    		);
	    	}
	    }
	    
	    $headers = array(
	    	'Accept: application/json',
			'Content-Type: application/json'
		);
		
		if($this->EE->extensions->active_hook('escort_pre_send'))
		{
			$content = $this->EE->extensions->call('escort_pre_send', 'postageapp', $content);
		}
				
		if(!function_exists('json_encode'))
		{
			$this->EE->load->library('Services_json');
		}
		$content = json_encode($content);
		
		return $this->_curl_request('https://api.postageapp.com/v.1.0/send_message.json', $headers, $content);
	}
	
	
	function _send_postmark($api_key)
	{	
	   	$email = array(
	    	'From' => $this->_recipient_str($this->email_out['from'], true),
	    	'To' => $this->_recipient_str($this->email_out['to']),
	    	'Subject' => $this->email_out['subject'],
	    	'Headers' => array($this->email_out['headers'])
	    );
	    if(!empty($this->email_out['reply-to']))
	    {
	    	$email['ReplyTo'] = $this->_recipient_str($this->email_out['reply-to'], true);
	    }
	    if(!empty($this->email_out['cc']))
	    {
	    	$email['Cc'] = $this->_recipient_str($this->email_out['cc']);
	    }
	    if(!empty($this->email_out['bcc']))
	    {
	    	$email['Bcc'] = $this->_recipient_str($this->email_out['bcc']);
	    }
	    if(!empty($this->email_out['html']))
	    {
	    	$email['HtmlBody'] = $this->email_out['html'];
	    }
	    if(!empty($this->email_out['text']))
	    {
	    	$email['TextBody'] = $this->email_out['text'];
	    }
	    if(!empty($this->email_out['attachments']))
	    {
	    	foreach($this->email_out['attachments'] as $attachment)
	    	{
	    		$email['Attachments'][] = array(
	    			'Name' => $attachment['name'],
	    			'ContentType' => $attachment['type'],
	    			'Content' => $attachment['content']
	    		);
	    	}
	    }

		$headers = array(
	    	'Accept: application/json',
			'Content-Type: application/json',
			'X-Postmark-Server-Token: '.$api_key
		);
		
		if($this->EE->extensions->active_hook('escort_pre_send'))
		{
			$email = $this->EE->extensions->call('escort_pre_send', 'postmark', $email);
		}
		
		if(!function_exists('json_encode'))
		{
			$this->EE->load->library('Services_json');
		}		
		$email = json_encode($email);
		
		return $this->_curl_request('http://api.postmarkapp.com/email', $headers, $email);
	}	
	
	
	function _send_sendgrid($username, $password)
	{
		$email = $this->email_out;
		$email['api_user'] = $username;
		$email['api_key'] = $password;
		
		if(!function_exists('json_encode'))
		{
			$this->EE->load->library('Services_json');
		}
		$email['headers'] = json_encode($email['headers']);
		
		if(!empty($email['from']['name']))
		{
			$email['fromname'] = $email['from']['name'];		
		}
		$email['from'] = $email['from']['email'];
	    
	    if(!empty($email['reply-to']))
	    {
	    	$email['replyto'] = $email['reply-to']['email'];
	    	unset($email['reply-to']);
	    }
	    
	    // SendGrid does not support CC
	    if(!empty($email['cc']))
	    {
	    	$email['to'] = array_merge($email['cc'], $email['to']);
	    }
	    	    
	    if(!empty($email['attachments']))
	    {
	    	$attachments = $this->_write_attachments();
	    	unset($email['attachments']);	    	
	    	foreach($attachments as $name => $path)
	    	{
	    		$email['files'][$name] = '@'.$path;
	    	}
	    }
	    
	    if($this->EE->extensions->active_hook('escort_pre_send'))
		{
			$email = $this->EE->extensions->call('escort_pre_send', 'sendgrid', $email);
		}
	    
	    $post = array();
	    $this->_http_build_post($email, $post);

		return $this->_curl_request('https://sendgrid.com/api/mail.send.json', null, $post);
		
	}

	
	/**
		Ultimately sends the email to each server.
	**/	
	function _curl_request($server, $headers = array(), $content, $htpw = null)
	{	
		$ch = curl_init($server);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		if(!empty($headers))
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		if(!empty($htpw))
		{
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $htpw);
		}
		$status = curl_exec($ch);
		// echo $status; exit();
		$curl_error = curl_error($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		return ($http_code != 200) ? false : true;
	}	
	

	/**
		Remove the Q encoding from our subject line
	**/
	function _decode_q($subject)
	{
	    $r = '';
	    $lines = preg_split('/['.$this->email_crlf.']+/', $subject); // split multi-line subjects
		foreach($lines as $line)
	    { 
	        $str = '';
	        // $line = str_replace('=9', '', $line); // Replace encoded tabs which ratch the decoding
	        $parts = imap_mime_header_decode(trim($line)); // split and decode by charset
	        foreach($parts as $part)
	        {
	            $str .= $part->text; // append sub-parts of line together
	        }
	        $r .= $str; // append to whole subject
	    }
	    
	    return $r;
	    // return utf8_encode($r);
	}
	
	
	/**
		Breaks the PITA MIME message we receive into its constituent parts
	**/
	function _body_and_attachments()
	{
		if($this->protocol == 'mail')
		{
			// The 'mail' protocol sets Content-Type in the headers
			if(strpos($this->email_in['header_str'], "Content-Type: text/plain") !== false)
			{	
				$this->email_out['text'] = $this->email_in['finalbody'];
			}
			elseif(strpos($this->email_in['header_str'], "Content-Type: text/html") !== false)
			{
				$this->email_out['html'] = $this->email_in['finalbody'];
			}
			else
			{
				preg_match('/Content-Type: multipart\/[^;]+;\s*boundary="([^"]+)"/i', $this->email_in['header_str'], $matches);
			}
		}	
		else
		{
			// SMTP and sendmail will set Content-Type in the body
			if(stripos($this->email_in['finalbody'], "Content-Type: text/plain") === 0)
			{	
				$this->email_out['text'] = $this->_clean_chunk($this->email_in['finalbody']);
			}
			elseif(stripos($this->email_in['finalbody'], "Content-Type: text/html") === 0)
			{
				$this->email_out['html'] = $this->_clean_chunk($this->email_in['finalbody']);
			}
			else
			{
				preg_match('/^Content-Type: multipart\/[^;]+;\s*boundary="([^"]+)"/i', $this->email_in['finalbody'], $matches);
			}
		}	
		
		// Extract content and attachments from multipart messages
		if(!empty($matches) && !empty($matches[1]))
		{
			$boundary = $matches[1];
			$chunks = explode('--' . $boundary, $this->email_in['finalbody']);
			foreach($chunks as $chunk)
			{
				if(stristr($chunk, "Content-Type: text/plain") !== false)
				{
					$this->email_out['text'] = $this->_clean_chunk($chunk);
				}
				
				if(stristr($chunk, "Content-Type: text/html") !== false)
				{
					$this->email_out['html'] = $this->_clean_chunk($chunk);
				}
				
				// Attachments
				if(stristr($chunk, "Content-Disposition: attachment") !== false)
				{
					preg_match('/Content-Type: (.*?); name=["|\'](.*?)["|\']/is', $chunk, $attachment_matches);
					if(!empty($attachment_matches))
					{
						if(!empty($attachment_matches[1]))
						{
							$type = $attachment_matches[1];
						}
						if(!empty($attachment_matches[2]))
						{
							$name = $attachment_matches[2];
						}
						$attachment = array(
							'type' => trim($type),
							'name' => trim($name),
							'content' => $this->_clean_chunk($chunk)
						);
						$this->email_out['attachments'][] = $attachment;
					}
				}
				
				if(stristr($chunk, "Content-Type: multipart") !== false)
				{
					// Another multipart chunk - contains the HTML and Text messages, here because we also have attachments
					preg_match('/Content-Type: multipart\/[^;]+;\s*boundary="([^"]+)"/i', $chunk, $inner_matches);
					if(!empty($inner_matches) && !empty($inner_matches[1]))
					{
						$inner_boundary = $inner_matches[1];
						$inner_chunks = explode('--' . $inner_boundary, $chunk);
						foreach($inner_chunks as $inner_chunk)
						{
							if(stristr($inner_chunk, "Content-Type: text/plain") !== false)
							{
								$this->email_out['text'] = $this->_clean_chunk($inner_chunk);
							}
							
							if(stristr($inner_chunk, "Content-Type: text/html") !== false)
							{
								$this->email_out['html'] = $this->_clean_chunk($inner_chunk);
							}
						}
					}
				}
			}
		}
		
		if(!empty($this->email_out['html']))
		{
			$this->email_out['html'] = $this->_clean_body($this->email_out['html']);
		}	

		if(!empty($this->email_out['text']))
		{
			$this->email_out['text'] = $this->_clean_body($this->email_out['text']);

		}	
	}
	

	/**
		HTML messages are being run through _prep_quoted_printable() in the CI Email library.
		Our services do not expect this, so we undo the damage.
	**/	
	function _clean_body($content)
	{
		$r = str_replace(array("=3D", "=20\n", "=\n", "=09\n"), array('=', ' ', '', ''), $content);
		return $r;
		// return utf8_encode($r);
		/*
			This was mangling ACT= portions of links.
			return utf8_encode(quoted_printable_decode($content));
		*/
	}
	

	/**
		Explodes a string which contains either a name and email address or just an email address into an array
	**/
	function _name_and_email($str)
	{
		$r = array(
			'name' => '',
			'email' => ''
		);
		
		$str = str_replace('"', '', $str);
		if(preg_match('/<([^>]+)>/', $str, $email_matches))
		{
			$r['email'] = trim($email_matches[1]);
			$str = trim(preg_replace('/<([^>]+)>/', '', $str));
			if(!empty($str) && $str != $r['email'])
			{
				$r['name'] = utf8_encode($str);
			}
		}
		else
		{
			$r['email'] = trim($str);
		}
		return $r;
	}
	
	/**
		Explodes a comma-delimited string of email addresses into an array
	**/	
	function _recipient_array($recipient_str)
	{
		$recipients = explode(',', $recipient_str);
		$r = array();
		foreach($recipients as $recipient)
		{
			$r[] = trim($recipient);
		}
		return $r;
	}
	
	/**
		Implodes an array of email addresses and names into a comma-delimited string
	**/		
	function _recipient_str($recipient_array, $singular = false)
	{
		if($singular == true)
		{
			if(empty($recipient_array['name']))
			{
				return $recipient_array['email'];
			}
			else
			{
				return $recipient_array['name'].' <'.$recipient_array['email'].'>';
			}
		}
		$r = array();
		foreach($recipient_array as $k => $recipient)
		{
			if(!is_array($recipient))
			{
				$r[] = $recipient;
			}
			else
			{
				if(empty($recipient['name']))
				{
					$r[] = $recipient['email'];
				}
				else
				{
					$r[] = $recipient['name'].' <'.$recipient['email'].'>';
				}
			}
		}
		return implode(',', $r);
	}
	
	/**
		Removes cruft from a multipart message chunk
	**/		
	function _clean_chunk($chunk)
	{
		return trim(preg_replace("/Content-(Type|ID|Disposition|Transfer-Encoding):.*?".NL."/is", "", $chunk));
	}
	
	
	/**
		Finds the temp directory, works for pre-PHP 5.2 as well
	**/	
	function _get_temp()
	{
		if(!function_exists('sys_get_temp_dir'))
		{
			function sys_get_temp_dir()
			{
				if(!empty($_ENV['TMP']))
				{
					return realpath($_ENV['TMP']);
				}
				if(!empty($_ENV['TMPDIR']))
				{
					return realpath( $_ENV['TMPDIR']);
				}
				if(!empty($_ENV['TEMP']))
				{
					return realpath( $_ENV['TEMP']);
				}
				
				$tempfile = tempnam(__FILE__, '');
				if(file_exists($tempfile))
				{
					unlink($tempfile);
					return realpath(dirname($tempfile));
				}
				return null;
			}
		}
		return realpath(sys_get_temp_dir());
	}
	
	/**
		Writes our array of base64-encoded attachments into actual files in the tmp directory
	**/		
	function _write_attachments()
	{
		$r = array();
		$this->EE->load->helper('file');
    	foreach($this->email_out['attachments'] as $attachment)
    	{
    		if(write_file($this->_get_temp().'/'.$attachment['name'], base64_decode($attachment['content'])))
    		{
    			$r[$attachment['name']] = $this->_get_temp().'/'.$attachment['name'];
    		}
    	}
    	return $r;
	}
	
	/**
		Translates a multi-dimensional array into the odd kind of array expected by cURL post
	**/		
	function _http_build_post($arrays, &$new = array(), $prefix = null)
	{	
	    foreach($arrays as $key => $value)
	    {
		    $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
	        if(is_array($value))
	        {
	            $this->_http_build_post($value, $new, $k);
	        }
	        else
	        {
	            $new[$k] = $value;
	        }
	    }
	}
	
	
	function _debug($value, $exit = true)
	{
		echo '<html><meta charset="UTF-8"><body><pre><code>';
		if(is_array($value))
		{
			print_r($value);
		}
		else
		{
			echo($value);
		}
		echo '</code></pre></body></html>';
		if($exit) exit();
	}
	

	function activate_extension()
	{
		$this->settings = array();
		
		$data = array(
			'class'		=> __CLASS__,
			'method'	=> 'email_send',
			'hook'		=> 'email_send',
			'settings'	=> serialize($this->settings),
			'version'	=> $this->version,
			'enabled'	=> 'y'
		);

		$this->EE->db->insert('extensions', $data);			
		
	}	
	

	function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}


	function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}
	}	
	

}