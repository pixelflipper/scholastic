<meta data-set="licenseKey" data-value="<?php echo $settings['license_key']; ?>">
<meta data-set="phoneHome" data-value="<?php if ($settings['phone_home']) { echo 'true'; } else { echo 'false'; } ?>">
<form method="post">
<input type="hidden" name="<?php echo ee()->security->get_csrf_token_name(); ?>" value="<?php echo ee()->security->get_csrf_hash(); ?>" />
<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				Your license key (from Devot:ee)
			</td>
			<td>
				<input type="text" name="license_key" value="<?php echo $settings['license_key'] ?>">
			</td>
		</tr>
	</tbody>
</table>
<div style="text-align: right;">
<input type="submit" name="submit" value="Update" class="submit">
</div>
</form>