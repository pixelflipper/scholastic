<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(	

	/* 
		Required for ADD-ONS > MODULES page
	*/
	
	'ajw_import_members_module_name' => 'AJW Import Members',
	'ajw_import_members_module_description' => 'Member import module',

	/* 
		Required for CONTROL PANEL pages
	*/

	'ajw_import_members_index' => 'Import Members',
	'ajw_import_members_edit' => 'Edit import',
	'ajw_import_members_settings' => 'Settings',
	'ajw_import_members_configure' => 'Configure import',
	'ajw_import_members_import' => 'Import',
	
	'ajw_import_members_id' => 'ID',
	'ajw_import_members_title' => 'Title',
	'ajw_import_members_description' => 'Must only contain letters',

	'ajw_import_members_confirm' => 'Do you really want to do this...',
	'ajw_import_members_successful' => 'Action successful',
	'ajw_import_members_added' => 'Item added',
	'ajw_import_members_updated' => 'Item updated',
	
	/* 
		Required for FRONT END module
	*/

	// None, yet...

	'' => ''
);

/* End of file lang.ajw_import_members.php */
/* Location: /system/expressionengine/third_party/ajw_import_members/language/english/lang.ajw_import_members.php */
