<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

// Include configuration
include(PATH_THIRD . 'category_construct/config.php');

/**
 * Category Construct Control Panel Class
 *
 * @package    category_construct
 * @author     TJ Draper <tj@buzzingpixel.com>
 * @link       https://buzzingpixel.com/ee-add-ons/category-construct
 * @copyright  Copyright (c) 2015, BuzzingPixel
 */
class Category_construct_mcp {

	/*------------------------------------*\
		# Public Methods
	\*------------------------------------*/

	/**
	 * Constructor
	 *
	 * @access      public
	 */
	public function __construct()
	{
		// Load the model
		ee()->load->model('category_construct_model');

		// Make sure all category sorting is set to custom
		ee()->category_construct_model->setCustomSorting();

		// Set class variables
		$this->settings = ee()->category_construct_model->getSettings();

		$this->modulesUrl = BASE . AMP . 'C=addons_modules';

		$this->baseUrl = $this->modulesUrl . AMP .
			'M=show_module_cp' . AMP . 'module=category_construct';

		$this->methodUrl = $this->baseUrl . AMP . 'method=';

		$this->groupId = ee()->session->userdata('group_id');

		$this->themesUrl = URL_THIRD_THEMES . 'category_construct/';

		// Set initial breadcrumb to try to provide some consistentcy
		ee()->cp->set_breadcrumb($this->modulesUrl, 'Modules');

		// Set JS
		ee()->cp->load_package_js('lib/chosen.jquery.min');
		ee()->cp->load_package_js('lib/jquery.mjs.nestedSortable.min');
		ee()->cp->load_package_js('construct_functions.min');
		ee()->cp->load_package_js('common.min');

		// Set CSS
		ee()->cp->add_to_head(
			'<link rel="stylesheet" href="' .
			$this->themesUrl .
			'chosen.min.css">'
		);
		ee()->cp->load_package_css('category_construct.min');

		ee()->cp->set_right_nav(array(
			'Groups' => $this->baseUrl,
			'License' => $this->baseUrl . AMP . 'method=license'
		));

		// Set vars we always want in the views
		$this->vars = array(
			'base_url' => $this->baseUrl,
			'method_url' => $this->methodUrl,
			'themes_url' => $this->themesUrl,
			'settings' => $this->settings
		);
	}

	/**
	 * Module Home Page
	 *
	 * @access      public
	 * @return      string
	 */
	public function index()
	{
		// Set page title
		ee()->view->cp_page_title = lang('home_page_title');

		// Get category groups
		$categoryGroups = ee()->category_construct_model->getCategoryGroups();

		// Remove any groups user does not have access to
		if ($this->groupId !== '1') {
			foreach ($categoryGroups as $key => $val) {
				$canEdit = explode('|', $val['can_edit_categories']);

				if (! in_array($this->groupId, $canEdit)) {
					unset($categoryGroups[$key]);
				}
			}

			$categoryGroups = array_values($categoryGroups);
		}

		// Set vars for the view
		$this->vars['category_groups'] = $categoryGroups;

		// Return the view
		return ee()->load->view('index', $this->vars, true);
	}

	/**
	 * Category Group Page
	 *
	 * @access      public
	 * @return      string
	 */
	public function show_group()
	{
		// Get group ID
		$groupId = ee()->input->get('id');

		// Get info abou the category group
		$categoryGroup = ee()->category_construct_model->getCategoryGroups(array(
			'groupId' => $groupId,
			'singleResult' => true
		));

		// Set the breadcrumb
		ee()->cp->set_breadcrumb($this->baseUrl, 'Category Construct Groups');

		// Set page title
		ee()->view->cp_page_title = lang('cat_group_page_title_prefix') . ': ' . $categoryGroup['group_name'];

		// Set page JS
		ee()->cp->load_package_js('construct_category_groups.min');

		// Get category groups
		$categories = ee()->category_construct_model->getCategories(array(
			'groupId' => $groupId
		));

		// Set the user permissions
		if ($this->groupId !== '1') {
			$editors = explode('|', $categoryGroup['can_delete_categories']);

			$canEdit = in_array($this->groupId, $editors);
		} else {
			$canEdit = true;
		}

		// Set vars for the view
		$this->vars['category_group'] = $categoryGroup;
		$this->vars['categories'] = $categories;
		$this->vars['can_edit'] = $canEdit;

		// Return the view
		return ee()->load->view('group', $this->vars, true);
	}

	/**
	 * Module License Page
	 *
	 * @access      public
	 * @return      string
	 */
	public function license()
	{
		// Set page title
		ee()->view->cp_page_title = 'Category Construct License';

		// Set the breadcrumb
		ee()->cp->set_breadcrumb($this->baseUrl, 'Category Construct Groups');

		if (ee()->input->post('license_key')) {
			ee()->category_construct_model->setLicenseKey(
				ee()->input->post('license_key')
			);

			$this->vars['settings']['license_key'] = ee()->input->post('license_key');
			$this->vars['settings']['phone_home'] = true;
		}

		// Return the view
		return ee()->load->view('license', $this->vars, true);
	}





	/*------------------------------------*\
		# Ajax
	\*------------------------------------*/

	/**
	 * Module Add Category Overlay Ajax Response
	 *
	 * @access      public
	 * @return      void
	 */
	public function add_cat_overlay()
	{
		exit(ee()->load->view('add_cat_overlay', false, true));
	}

	/**
	 * Module Insert Category Ajax Response
	 *
	 * @access      public
	 * @return      void
	 */
	public function insert_cat()
	{
		// Get input vars
		$group = ee()->input->get('group');
		$categoryName = ee()->input->post('category_name');
		$categorySlug = ee()->input->post('category_url_title');
		$categoryDescription = ee()->input->post('category_description');

		// Insert the category and get the result
		$insertResult = ee()->category_construct_model->insertCategory(array(
			'group_id' => $group,
			'cat_name' => $categoryName,
			'cat_url_title' => $categorySlug,
			'cat_description' => $categoryDescription
		), true);

		// Set the vars
		$this->vars = $this->vars + $insertResult;

		// Return the view
		exit(ee()->load->view('category', $this->vars, true));
	}

	/**
	 * Module Update Categories Ajax Response
	 *
	 * @access      public
	 * @return      void
	 */
	public function update_cats()
	{
		// Get the save data from the post data
		$saveData = ee()->input->post('save_data');

		// Send the data to the model for insertion
		ee()->category_construct_model->updateCats($saveData);

		// Send the success object json response
		ee()->output->send_ajax_response(array('success' => 1));
	}

	/**
	 * Module Delete Categories Ajax Response
	 *
	 * @access      public
	 * @return      void
	 */
	public function delete_cat()
	{
		// Get the cat ID from the post data
		$catId = ee()->input->post('cat_id');

		// Send the ID to the model for deletion
		ee()->category_construct_model->deleteCats($catId);

		// Send the success object json response
		ee()->output->send_ajax_response(array('success' => 1));
	}

	/**
	 * Module Update Categories Ajax Response
	 *
	 * @access      public
	 * @return      void
	 */
	public function update_cat()
	{
		// Get the save data from the post data
		$saveData = ee()->input->post('save_data');

		// Send the data to the model for insertion
		$nodeData = ee()->category_construct_model->updateCats($saveData);

		// Send the success object json response
		ee()->output->send_ajax_response(array('success' => 1));
	}
}