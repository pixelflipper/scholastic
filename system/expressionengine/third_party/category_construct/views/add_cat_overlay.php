<form class="construct-overlay construct-overlay--add-cat js-construct-overlay">
	<div class="construct-overlay__input-wrapper">
		<label class="construct-overlay__label" for="category_name">
			* Category Name
		</label>
		<input
			type="text"
			name="category_name"
			id="category_name"
			class="construct-overlay__input js-construct-add-name js-construct-required"
			required
		>
	</div>
	<div class="construct-overlay__input-wrapper">
		<label class="construct-overlay__label" for="category_url_title">
			* Category Slug
		</label>
		<input
			type="text"
			name="category_url_title"
			id="category_url_title"
			class="construct-overlay__input js-construct-add-slug js-construct-required"
			required
		>
	</div>
	<div class="construct-overlay__input-wrapper">
		<label class="construct-overlay__label" for="category_description">
			Category Description
		</label>
		<textarea
			name="category_description"
			id="category_description"
			class="construct-overlay__textarea"
		></textarea>
	</div>
	<div class="construct-overlay__input-wrapper construct-overlay__input-wrapper--buttons">
		<a class="submit construct-overlay__cancel js-construct-cancel">Cancel</a>
		<button
			class="submit construct-overlay__submit js-construct-submit"
			data-text-value="Add Category"
		>
			Add Category
		</button>
	</div>
</form>