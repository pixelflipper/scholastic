<?php if (! $settings['license_key']) { ?>
	<p class="construct-no-license">
		<?php echo lang('no_license_key'); ?>
	</p>
<?php } ?>

<meta data-set="licenseKey" data-value="<?php echo $settings['license_key']; ?>">
<meta data-set="phoneHome" data-value="<?php if ($settings['phone_home']) { echo 'true'; } else { echo 'false'; } ?>">
<?php if ($category_groups) { ?>
	<div class="construct-category-groups">
		<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th class="construct-category-groups__id-column">
						ID
					</th>
					<th class="construct-category-groups__name-column">
						Group Name
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($category_groups as $category_group) { ?>
					<tr>
						<td>
							<?php echo $category_group['group_id']; ?>
						</td>
						<td>
							<a href="<?php echo $method_url; ?>show_group&amp;id=<?php echo $category_group['group_id']; ?>">
								<?php echo $category_group['group_name']; ?>
							</a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="construct-no-groups">
		<p>There are currently no category groups you have access to.</p>
	</div>
<?php } ?>