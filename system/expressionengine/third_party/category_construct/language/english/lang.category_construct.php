<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

include(PATH_THIRD . 'category_construct/config.php');

$lang = array(
	// Global
	'category_construct_module_name' => CATEGORY_CONSTRUCT_NAME,
	'category_construct_module_description' => CATEGORY_CONSTRUCT_DESC,

	// CP Page Titles
	'home_page_title' => 'Category Construct Groups',
	'cat_group_page_title_prefix' => 'Category Group',

	// Views
	'no_categories_in_group' => 'You don&rsquo;t have any Categories in this group yet. Click the &ldquo;Plus Button&rdquo; to get started.',

	// No license key
	'no_license_key' => 'You have not entered a license key. Please enter the licene key fron your purchase on Devot:ee.'
);