<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(
	'escort_intro' => 'Activate as many services as you wish, and drag them into your preferred order. If your preferred service fails for any reason, the next active service will be used. If all services fail, your email will be sent via ExpressionEngine&rsquo;s default email method.',
	'escort_config_warning' => 'You appear to have Escort configured via config.php, so changes you make here may not be saved.',
	'escort_enable' => 'Enable',
	'escort_save_settings' => 'Save Settings',	'escort_mandrill_name' => 'Mandrill',
	'escort_mandrill_description' => 'Mandrill is run by MailChimp, and offers 12,000 emails sends per month on their free plan. Sign-up at <a href="http://mandrill.com">http://mandrill.com</a>.',
	'escort_mandrill_api_key' => 'API Key',
	'escort_mandrill_subaccount' => 'Subaccount (optional)',
	'escort_postmark_name' => 'Postmark',
	'escort_postmark_description' => 'Postmark offers 1000 email sends per month on their free plan. Sign-up at <a href="http://postmarkapp.com">http://postmarkapp.com</a>. <strong>Note:</strong> the From address of each email must already be verified in your Postmark dashboard to send succesfully.',
	'escort_postmark_api_key' => 'API Key',	
	'escort_postageapp_name' => 'PostageApp',
	'escort_postageapp_description' => 'PostageApp offers 100 email sends per day on their free plan. Sign-up at <a href="http://postageapp.com">http://postageapp.com</a>.',
	'escort_postageapp_api_key' => 'API Key',
	'escort_sendgrid_name' => 'SendGrid',
	'escort_sendgrid_description' => 'SendGrid offers 200 email sends per day on their free plan. Sign-up at <a href="http://sendgrid.com">http://sendgrid.com</a>.',
	'escort_sendgrid_username' => 'Username',	
	'escort_sendgrid_password' => 'Password',	
	'escort_mailgun_name' => 'MailGun',
	'escort_mailgun_description' => 'MailGun is run by RackSpace, and offers 200 email sends per day on their free plan. Sign-up at <a href="http://mailgun.com">http://mailgun.com</a>.',
	'escort_mailgun_api_key' => 'API Key',	
	'escort_mailgun_domain' => 'Domain Name',	
);

/* End of file lang.email_test.php */
/* Location: /system/expressionengine/third_party/email_test/language/english/lang.email_test.php */
