<p>If you find this module useful, and if it saves you time, please consider making a donation. Thanks, Andrew</p>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="GRR4DR34QSS6Y">
<input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>

<br/>
<br/>

<?php 

$this->table->set_template($cp_table_template);
$this->table->set_heading("Results");

$this->table->add_row(
	$results
);

echo $this->table->generate();

?>

<?php if( $id == 0 ): ?>

	<p>

		<?php echo form_open( $form_action ); ?>
		<?php echo form_hidden( 'id', 0 ); ?>
		<input type="submit" value="Save" class="submit" />
		<?php echo form_close(); ?>

	</p>

<?php else: ?>

	<p>

		<?php echo form_open( $form_action ); ?>
		<?php echo form_hidden( 'id',  $id ); ?>
		<input type="submit" value="Update saved import" class="submit" />
		<?php echo form_close(); ?>

	</p>
	<p>

		<?php echo form_open( $form_action ); ?>
		<?php echo form_hidden( 'id', 0 ); ?>
		<input type="submit" value="Save as new import" class="submit" />
		<?php echo form_close(); ?>

	</p>

<?php endif; ?>

