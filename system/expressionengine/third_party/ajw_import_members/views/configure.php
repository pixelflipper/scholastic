<?php if( isset( $errors ) && count( $errors ) ) {
	foreach( $errors as $error ) {
		echo '<p class="notice">Error: ' . $error . '</p>';
	}
}
?>

<?php 
	echo form_open( $form_action ); 
	echo form_hidden( "step", "configure" ); 
?>

<?php 

$this->table->set_template($cp_table_template);
$this->table->set_heading("Default Fields", "Value");

$this->table->add_row(
	array(
		'colspan' => 2,
		'data' => 'Choose which values to use for the standard member fields',
		'class' => 'box'
	)
);

/* Standard fields */

$this->table->add_row(
	form_label('Email', 'email') .
	'<div class="subtext">The members\'s email address.</div>', 
	form_dropdown("email", $data_fields, 
		isset($default_settings["config"]["email"]) ? $default_settings["config"]["email"] : '' )
);

$this->table->add_row(
	form_label('Screen name', 'screen_name') .
	'<div class="subtext">The members\'s screen name.</div>', 
	form_dropdown("screen_name", $data_fields, 
		isset($default_settings["config"]["screen_name"]) ? $default_settings["config"]["screen_name"] : '' )
);

$this->table->add_row(
	form_label('Username', 'username') .
	'<div class="subtext">The members\'s username.</div>', 
	form_dropdown("username", $data_fields, 
		isset($default_settings["config"]["username"]) ? $default_settings["config"]["username"] : '' )
);


echo $this->table->generate();
echo $this->table->clear();


$this->table->set_template($cp_table_template);
$this->table->set_heading("Password", "Value");

$this->table->add_row(
	array(
		'colspan' => 2,
		'data' => 'Choose which values to use for the password',
		'class' => 'box'
	)
);

$this->table->add_row(
	form_label('Password', 'password') .
	'<div class="subtext">If this is left blank a password will be generated.</div>', 
	form_dropdown("password", $data_fields, 
		isset($default_settings["config"]["password"]) ? $default_settings["config"]["password"] : '' )
);

// plain text, md5, sha?
$this->table->add_row(
	form_label('Password Type', 'password_type') .
	'<div class="subtext">If the password is provided, select the password\'s encryption method</div>', 
	form_dropdown("password_type", array( "text" => "None (plain text)", "md5" =>"MD5", "sha1" => "SHA1" ), 
		isset($default_settings["config"]["password_type"]) ? $default_settings["config"]["password_type"] : '' )
);

echo $this->table->generate();
echo $this->table->clear();

$this->table->set_template($cp_table_template);
$this->table->set_heading("Member Group Fields", "Value");

$this->table->add_row(
	array(
		'colspan' => 2,
		'data' => 'Choose which values to use for the member group',
		'class' => 'box'
	)
);

// set default group
$this->table->add_row(
	form_label('Default Member Group', 'group_id') .
	'<div class="subtext">New members will be placed in this member group</div>', 
	form_dropdown("group_id", $member_groups, 
		isset($default_settings["config"]["group_id"]) ? $default_settings["config"]["group_id"] : 5 )
);
// override with data
$this->table->add_row(
	form_label('Member Group from data', 'member_group') .
	'<div class="subtext">The default member group an be overridden by a data field</div>', 
	form_dropdown("member_group", $data_fields, 
		isset($default_settings["config"]["member_group"]) ? $default_settings["config"]["member_group"] : '' )
);


echo $this->table->generate();
echo $this->table->clear();

$this->table->set_template($cp_table_template);
$this->table->set_heading("Custom Fields", "Value");

$this->table->add_row(
	array(
		'colspan' => 2,
		'data' => 'Choose which values to use for the custom member fields',
		'class' => 'box'
	)
);

foreach( $custom_member_fields as $field_id => $field_label ) {
	
	$m_field_id = "m_field_id_" . $field_id;
	
	$this->table->add_row(
		form_label($field_label, $m_field_id),
		form_dropdown($m_field_id, $data_fields, 
			isset($default_settings["config"][$m_field_id]) ? $default_settings["config"][$m_field_id] : '' )
	);
	
}

echo $this->table->generate();
echo $this->table->clear();

$this->table->set_template($cp_table_template);
$this->table->set_heading("Standard Member Fields", "Value");

$this->table->add_row(
	array(
		'colspan' => 2,
		'data' => 'Choose which values to use for the standard member fields',
		'class' => 'box'
	)
);

/* Standard fields */

foreach( $standard_fields as $field_id => $field_label ) {
	
	$this->table->add_row(
		form_label($field_label, $field_id),
		form_dropdown($field_id, $data_fields, 
			isset($default_settings["config"][$field_id]) ? $default_settings["config"][$field_id] : '' )
	);
	
}


echo $this->table->generate();
echo $this->table->clear();





?>

<p style="float:left"><input type="submit" name="import" value="Do Import" class="submit" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="<?php echo $back_link ?>">Back to Settings</a></p>

<?php
if( isset( $id ) && $id != "" ) {
	echo form_hidden("id", $id);
}
?>

<p style="float:right"><input type="submit" name="save" value="Save import" class="submit" /></p>

<?php echo form_close(); ?>


