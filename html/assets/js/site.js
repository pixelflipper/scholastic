$(document).ready(function() {
    $('audio').mediaelementplayer({
        // if the <video width> is not specified, this is the default
        defaultVideoWidth: 480,
        // if the <video height> is not specified, this is the default
        defaultVideoHeight: 270,
        // if set, overrides <video width>
        videoWidth: -1,
        // if set, overrides <video height>
        videoHeight: -1,
        // width of audio player
        audioWidth: 400,
        // height of audio player
        audioHeight: 30,
        // initial volume when the player starts
        startVolume: 0.8,
        // useful for <audio> player loops
        loop: false,
        // enables Flash and Silverlight to resize to content size
        enableAutosize: true,
        // the order of controls you want on the control bar (and other plugins below)
        features: ['playpause','progress','current','duration','tracks','volume','fullscreen'],
        // Hide controls when playing and mouse is not over the video
        alwaysShowControls: false,
        // force iPad's native controls
        iPadUseNativeControls: false,
        // force iPhone's native controls
        iPhoneUseNativeControls: false, 
        // force Android's native controls
        AndroidUseNativeControls: false,
        // forces the hour marker (##:00:00)
        alwaysShowHours: false,
        // show framecount in timecode (##:00:00:00)
        showTimecodeFrameCount: false,
        // used when showTimecodeFrameCount is set to true
        framesPerSecond: 25,
        // turns keyboard support on and off for this instance
        enableKeyboard: true,
        // when this player starts, it will pause other players
        pauseOtherPlayers: true,
        // array of keyboard commands
        keyActions: []
     
    });
    $('a#back-nav').click(function(){
        parent.history.back();
        return false;
    });

    $('[data-toggle="tooltip"], [data-toggle="tab tooltip"]').tooltip({
        trigger: 'hover',
            placement: 'top',
            animate: true,
            delay: 400,
            container: 'body'
    });
    
    var h = screen.height;
    var w = screen.width;
    console.log(h + " - " + w)
    $('.win-open').on('click', function (e) {
        window.open( $(this).attr('href'), '_blank', 'toolbar=no ,location=no, status=no, titlebar=no, menubar=no, width='+w +', height=' +h);
        e.preventDefault();
    });
    
    $('body').on('click', '.clickable', function () {
        window.open($(this).find('.clickable-link').attr('href'),'_blank');
        return false;
    });

    $('.clickable-self').click(function() {
        window.location = $(this).find('.clickable-link').attr('href');
        return false;
    });

    $('.nav-stacked li:first').addClass('active');

    $('.nav-stacked>li.level-1>a').click(function() {

        var curActive = $(this).parent().parent().find('li.level-1.active');
        var clickedLine = $(this).parent('li');
        // clicked on non-active parent
        if (!clickedLine.hasClass("active")) {
            //move stuff
            var newActive = clickedLine;                
            if (curActive.children('ul').length > 0 ) {
                curActive.children('ul').slideUp(function(){
                    curActive.removeClass("active");
                    //curActive.find(".level-2.active").removeClass("active");
                    newActive.addClass('active');
                    newActive.children('ul').slideDown();  
                });
            } else {
                curActive.removeClass("active");
                newActive.addClass('active');
                newActive.children('ul').slideDown();    
            }
        } // moved this up from below next if statement
            // get correct entries
            //if (clickedLine.hasClass("no-ent")) {
            if (clickedLine.hasClass("cat-0")) {
                // select child and get content
                var firstChild = clickedLine.find("li.level-2:first-child");
                firstChild.addClass("active");
                var cat = firstChild.find("a").data("cat");
                var type = firstChild.find("a").data("type");
                var link = $(this);
                var group = firstChild.find("a").data("group");
                var ord = firstChild.find("a").data("ord");
                getPaneContent(cat,link,type,ord,group);
                console.log("has no entries; cat - " + cat);
            } else {
                // show it's loose entries
                if (clickedLine.hasClass("sub-is-active")) {
                    $(this).parent('li').removeClass('sub-is-active');
                }
                var type = $(this).data("type");
                var cat = $(this).data("cat");
                var group = $(this).data("group");
                var ord = $(this).data("ord");
                var link = $(this);
                console.log("clicked level 1!!");
                getPaneContent(cat,link,type,ord,group);
            }
         
        return false;
    });
    
    $('.nav-stacked li li a').click(function() {
        $('.nav-stacked li li.active').removeClass("active");
        $(this).parent('li').addClass("active");  
        var cat = $(this).data("cat");
        var type = $(this).data("type");
        var group = $(this).data("group");
        var ord = $(this).data("ord");
        var link = $(this);
        getPaneContent(cat,link,type,ord,group);     
    });

    function setParentCat() {
        $('.nav-stacked li:first').addClass('active');
         $(this).data("cat");
    }
   
    function getPaneContent(cat,link,type,ord,group) {
        console.log("getting content. order: " + ord + "; group: " + group + "; type: " + type);
        var paneCon = link.closest('.tab-pane').find('.tab-pane-content');
        var paneConID = paneCon.attr('id');
        console.log("pane: " + paneCon.attr('id'));
        // var cat = $(this).data("cat");
        paneCon.fadeOut(500);
        //$(paneCon + ' #tab-loading').fadeIn(200);
        if (type == "ind-read") {
            $("#" + paneConID + " .video-js").each(function (videoIndex) {
                var videoId = $(this).attr("id");
                var myPlayerDis = videojs(videoId);
                myPlayerDis.dispose();
                console.log("video disposed: " + videoId);
            });
            paneCon.load("/ajax/tab-content-ir/" + cat, function () { //calback function
                //$('.tab-loading').fadeOut(200);
                console.log("IND READ");
                paneCon.fadeIn(500);
                $('.video-link').magnificPopup({
                    image: {
                        verticalFit: true
                    },
                    type:'inline',
                    midClick: true,
                    callbacks: {
                        open: function() {
                            var vid=this.content.find("video").attr('id');
                            var myPlayer = videojs(vid);
                            myPlayer.play();
                            console.log("popup opened");
                        },
                        close: function() {
                           console.log("popup closed");
                        }
                    }
                });
            });
        } else {
            if (ord == "custom") {
                //$("#tab-pane-content2 .video-js").each(function (videoIndex) {
                $("#" + paneConID + " .video-js").each(function (videoIndex) {
                    var videoId = $(this).attr("id");
                    var myPlayerDis = videojs(videoId);
                    myPlayerDis.dispose();
                    console.log("video disposed: " + videoId);
                });
               // videoDispose().done(function () {
                    paneCon.load("/ajax/tab-content-order/" + cat + "/group-" + group, function () { //calback function
                        //$('.tab-loading').fadeOut(200);
                        console.log("CUSTOM : " + ord);
                        paneCon.fadeIn(500);
                        $('.video-link').magnificPopup({
                            image: {
                                verticalFit: true
                            },
                            type:'inline',
                            midClick: true,
                            callbacks: {
                                open: function() {
                                    var vid=this.content.find("video").attr('id');
                                    var myPlayer = videojs(vid);
                                    myPlayer.play();
                                    console.log("popup opened");
                                },
                                close: function() {
                                   console.log("popup closed");
                                }
                            }
                        });
                    });
              //  });
            } else {
                // $.each(_V_.players, function (key, player) { 
                //     if (player.isReady) { player.dispose(); } 
                //     else { delete _V_.players[player.id]; } 
                //     console.log("alpha-disposed");
                // }); 
                //$("#" + paneConID + " .video-js").each(function (videoIndex) {

                $("#" + paneConID + " .video-js").each(function (videoIndex) {
                    var videoId = $(this).attr("id");
                    var myPlayerDis = videojs(videoId);
                    myPlayerDis.dispose();
                    console.log("video disposed: " + videoId);
                });
                paneCon.load("/ajax/tab-content/" + cat, function () { //calback function
                    //$('.tab-loading').fadeOut(200);
                    console.log("NORMAL :" + ord);
                    paneCon.fadeIn(500);
                    $('.video-link').magnificPopup({
                        image: {
                           verticalFit: true
                        },
                        type:'inline',
                        midClick: true,
                        callbacks: {
                            open: function() {
                                var vid=this.content.find("video").attr('id');
                                var myPlayer = videojs(vid);
                                myPlayer.play();
                                console.log("popup opened");
                            },
                            close: function() {
                                console.log("popup closed");
                            }
                        }
                    });
                });

            }
        }
    }
    function videoDispose(par) {
        //$("#" + par + " .video-js").each(function (videoIndex) {
        $("#tab-pane-content2 .video-js").each(function (videoIndex) {
            var videoId = $(this).attr("id");
            var myPlayerDis = videojs(videoId);
            myPlayerDis.dispose();
            console.log("video disposed: " + videoId);
        });
    }
 
    function switchTab() {
        $('.nav-stacked li.level-1:first-child').addClass("active");
        $('.nav-stacked li.level-1.cat-0').find("li:first").addClass("active");

    }
    
    switchTab();

    $('.subnav-tabs a').click(function (e) {
        e.preventDefault(e)
        $(this).tab('show');
    });
    $('.back-arrow-tabs').click(function(e){
        e.preventDefault(e);
        if ($('.subnav-tabs > .active').is(':first-child')) {
            var back = $(this).data("link");
            window.location.href = back;
        } else {
            $('.subnav-tabs > .active').prev('li').find('a').trigger('click');
        }
    });
    $('.next-arrow-tabs').click(function(e){
        e.preventDefault(e);
        if ($('.subnav-tabs > .active').is(':last-child')) {
            var next = $(this).data("link");
            window.location.href = next;
        } else {
            $('.subnav-tabs > .active').next('li').find('a').trigger('click');
        }
    });
    $('.circle-nav-tabs a').click(function (e) {
        e.preventDefault(e)
        $(this).tab('show');
    });

    $('.framework-chart-link').magnificPopup({
        verticalFit: true,
        type:'inline',
        midClick: true
    });

    $('.video-link').magnificPopup({
        image: {
            verticalFit: true
        },
        type:'inline',
        midClick: true,
        callbacks: {
            open: function() {
                var vid=this.content.find("video").attr('id');
                var myPlayer = videojs(vid);
                myPlayer.play();
                console.log("popup opened");
            },
            close: function() {
               console.log("popup closed");
            }
        }
    });
    

});