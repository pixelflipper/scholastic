<li
	class="construct__cat-item js-construct-item js-construct-item-<?php echo $cat_id; ?> js-construct-parent-<?php echo $parent_id; ?>"
	data-cat-id="<?php echo $cat_id; ?>"
	data-parent-id="<?php echo $parent_id; ?>"
>

	<?php if($parent_id) { ?>
		<meta data-set="catParents[]" data-value="<?php echo $parent_id; ?>">
	<?php } ?>

	<div class="construct__cat">

		<?php // Category Drag Handle ?>
		<div class="construct__cat-handle js-construct-handle">
			<div class="construct__cat-handle-inner">___</div>
			<div class="construct__cat-handle-inner">___</div>
			<div class="construct__cat-handle-inner">___</div>
		</div>

		<?php // Category Name ?>
		<div class="construct__cat-name-wrapper">
			<label class="construct__label" for="name">Name</label>
			<div
				class="construct__cat-text construct__cat-text--name js-cat-update-input js-cat-name-<?php echo $cat_id; ?>"
				data-update-name="cat_name"
			><?php echo $cat_name; ?></div>
		</div>

		<?php // Category Slug ?>
		<div class="construct__cat-slug-wrapper">
			<label class="construct__label" for="slug">Slug</label>
			<div
				class="construct__cat-text js-cat-update-input"
				data-update-name="cat_url_title"
			><?php echo $cat_url_title; ?></div>
		</div>

		<?php // Category Description ?>
		<div class="construct__cat-desc-wrapper">
			<label class="construct__label" for="desc">Description</label>
			<div
				class="construct__cat-text js-cat-update-input js-cat-desc"
				data-update-name="cat_description"
			><?php echo $cat_description; ?></div>
		</div>

		<?php // Category Delete ?>
		<a
			class="construct__cat-delete js-construct-cat-delete"
			style="background-image:url(<?php echo $themes_url; ?>construct-sprite.png)"
		>
			Delete Category
		</a>

	</div>
</li>