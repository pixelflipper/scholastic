<?php

if (! defined('CATEGORY_CONSTRUCT_NAME')) {
	define('CATEGORY_CONSTRUCT_NAME', 'Category Construct');
	define('CATEGORY_CONSTRUCT_VER', '1.1.0-rc.1');
	define('CATEGORY_CONSTRUCT_AUTHOR', 'TJ Draper');
	define('CATEGORY_CONSTRUCT_AUTHOR_URL', 'https://buzzingpixel.com');
	define('CATEGORY_CONSTRUCT_DESC', 'Organize and output categories in a sane manner');
	define('CATEGORY_CONSTRUCT_PATH', PATH_THIRD . 'category_construct/');
}

$config['name'] = CATEGORY_CONSTRUCT_NAME;
$config['version'] = CATEGORY_CONSTRUCT_VER;