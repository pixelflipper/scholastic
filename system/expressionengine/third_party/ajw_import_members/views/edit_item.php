<?php echo form_open( $update_item_action ); ?>

<?php 

// Generic form builder

if( isset( $settings["id"] ) ) {
	echo form_hidden("id", $settings["id"] );
}

$this->table->set_template($cp_table_template);
$this->table->set_heading("Settings", "Value");

foreach( $form as $form_name => $form_data ) {

	$form_label = '';
	if( isset( $form_data["required"] ) ) {
		$form_label .= '<em class="required">*</em> ';
	}
	$form_label .= form_label(lang( $form_data["label"] ), $form_name);
	if( isset( $form_data["notes"] ) ) {
		$form_label .= '<div class="subtext">'.lang($form_data["notes"]).'</div>';
	}
	$form_label .= form_error( $form_name );

	switch( $form_data["type"] ) {
		case "checkbox": {
			$form_input = form_checkbox($form_name, $form_data["value"], 
				(isset($default_settings[$form_name]) && $default_settings[$form_name] == $form_data["calue"] ? TRUE : FALSE) );
			break;
		}
		case "dropdown": {
			$form_input = form_dropdown( $form_name, $form_data["items"], 
				isset($default_settings[$form_name]) ? $default_settings[$form_name] : '' );
			break;
		}
		case "textarea" : {
			$form_input = form_textarea($form_name,  
				isset($settings[$form_name]) ? $settings[$form_name] : '' );
			break;
		}
		default: {
			$form_input = form_input($form_name,  
				isset($settings[$form_name]) ? $settings[$form_name] : '' );
		}
	}

	$this->table->add_row(
		$form_label,
		$form_input
	);
	
}

echo $this->table->generate();
echo $this->table->clear();

?>

<input type="submit" value="Update" class="submit" />

<?php echo form_close(); ?>