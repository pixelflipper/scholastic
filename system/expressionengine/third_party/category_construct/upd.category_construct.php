<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

// Include configuration
include(PATH_THIRD . 'category_construct/config.php');

/**
 * Category Construct Module Install/Update Class
 *
 * @package    category_construct
 * @author     TJ Draper <tj@buzzingpixel.com>
 * @link       https://buzzingpixel.com/ee-add-ons/category-construct
 * @copyright  Copyright (c) 2015, BuzzingPixel
 */
class Category_construct_upd {

	/*------------------------------------*\
		# Set class vars
	\*------------------------------------*/

	/**
	 * Module Name
	 *
	 * @access      public
	 * @var         string
	 */
	public $name = CATEGORY_CONSTRUCT_NAME;

	/**
	 * Module Version
	 *
	 * @access      public
	 * @var         string
	 */
	public $version = CATEGORY_CONSTRUCT_VER;


	/*------------------------------------*\
		# Public Methods
	\*------------------------------------*/

	/**
	 * Constructor
	 *
	 * @access      public
	 * @return      void
	 */
	public function __construct()
	{
		ee()->load->model('category_construct_install_model');
	}

	/**
	 * Install Method
	 *
	 * @access      public
	 * @return      bool
	 */
	public function install()
	{
		ee()->db->insert('modules', array(
			'module_name' => 'Category_construct',
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		));

		ee()->category_construct_install_model->insertSettingsTable();

		return true;
	}

	/**
	 * Uninstall Method
	 *
	 * @access      public
	 * @return      bool
	 */
	public function uninstall()
	{
		ee()->db->delete('modules', array(
			'module_name' => 'Category_construct'
		));

		ee()->category_construct_install_model->removeSettingsTable();

		return true;
	}

	/**
	 * Update Method
	 *
	 * @access      public
	 * @return      bool
	 */
	public function update($current = '')
	{
		// If beta or Release Candidate, add the phone home setting
		if (strpos($current, 'beta') or strpos($current, 'rc')) {
			ee()->category_construct_install_model->insertSettingsTable();
		}

		return true;
	}
}