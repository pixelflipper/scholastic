<?php if ( ! defined('EXT')) exit('Invalid file request.');

/**
 * AJW Simple Model
 *
 * @package ExpressionEngine
 * @subpackage Addons
 * @category Module
 * @author Andrew Weaver
 * @link http://brandnewbox.co.uk/
 */

class Ajw_import_members_model extends CI_Model {

	var $datatypes = array();
	var $settings = array();
	
	var $return_data = "";
	var $errors = array();

	function __construct() {
		parent::__construct(); 
	}
	
	/**
	 * Fetch items to display in a table
	 *
	 * @param string $settings 
	 * @return void
	 * @author Andrew Weaver
	 */
	function index( $settings=array() ) {

		$data = array();
		
		// Populate data array with, um..., data
		$data["headings"] = array("ajw_import_members_id", "ajw_import_members_title");
		$data["rows"] = array(
			array("1", "One"),
			array("2", "Two"),
			array("3", "Three")
		);
		$data["pagination"] = "";

		return $data;
	}

	/**
	 * Dummy fetch function
	 *
	 * @param string $id 
	 * @return void
	 * @author Andrew Weaver
	 */
	function fetch( $id ) {
		
		// In real life, probably fetch this from the database
		
		$data = array(
			"id" => $id,
			"title" => "Testing..."
		);
		
		return $data;
	}

	/**
	 * Define the elements in an edit form. Used to build the form and validate it.
	 *
	 * @return void
	 * @author Andrew Weaver
	 */
	function edit_item_form() {
		
		$data = array();
		
		$data = array(
			'title' => array(
				'required' => 'required|alpha',
				'label' => 'ajw_import_members_title',
				'notes' => 'ajw_import_members_description',
				'type' => 'input'
			)
		);
		
		return $data;
	}


	function fetch_datatype_names() {
		
		$this->initialise_types();

		$types = array();
		foreach( $this->datatypes as $type_name => $type ) {
			$types[ $type_name ] = $type->display_name();
		}
		
		return $types;
	}


	function initialise_types() {
		
		if ( ! class_exists('Datagrab_type') ) {
			require_once PATH_THIRD.'ajw_import_members/libraries/Datagrab_type'.EXT;
		}	
		
		$path = PATH_THIRD.'ajw_import_members/datatypes/';
		
		$dir = opendir($path);

		while (($folder = readdir($dir)) !== FALSE) {
			if( is_dir($path.$folder) && $folder != "." && $folder != ".." ) {
				$filename = "/dt." . $folder . EXT;
				if ( ! class_exists( $folder ) ) {
					if( file_exists( $path.$folder.$filename ) ) {
						include($path.$folder.$filename);
						if (class_exists($folder)) {
							$this->datatypes[$folder] = new $folder();
						}
					}
				}
			}
		}
		closedir($dir);
		
		ksort( $this->datatypes );
		
		return count( $this->datatypes );
		
	}

	function do_import( $datatype, $settings ) {
		
		$this->datatype = $datatype;
		$this->settings = $settings;

		// Set up the data source
		$this->initialise_types();
		$datatype->initialise( $this->settings );
		$datatype->fetch();
		
		// Don't save queries
		$this->db->save_queries = FALSE;

		// Loop over items
		$row_num = 0;
		while( $item = $datatype->next() ) {

			// Reset time out
			set_time_limit(30);

			// Check whether to skip this row or not
			$row_num++;
			if( isset($this->settings["datatype"]["skip"]) && is_numeric($this->settings["datatype"]["skip"]) ) {
				if ( $row_num <= $this->settings["datatype"]["skip"] ) {
					continue;
				}
			}

			// Initialise array to store entry data
			$data = array();

			$data["email"] = $datatype->get_item( $item, $this->settings["config"][ "email" ] );
			$data["screen_name"] = $datatype->get_item( $item, $this->settings["config"][ "screen_name" ] );
			$data["username"] = $datatype->get_item( $item, $this->settings["config"][ "username" ] );
		
			// Do member group
			// Get default group
			$data["group_id"] = $this->settings["config"][ "group_id" ];
			// Check whether we are overriding it with data
			if( $this->settings["config"]["member_group"] != "" ) {
				$data["group_id"] = $datatype->get_item( $item, $this->settings["config"][ "member_group" ] );
			}

			// Do password
			if( $this->settings["config"]["password"] == "" ) {
				// Use random password
				$data["password"] = hash( 'sha1', random_string('encrypt') );
			} else {
				// Fetch password from data
				$data["password"] = $datatype->get_item( $item, $this->settings["config"][ "password" ] );
				// Encrypt if is plain text
				if( $this->settings["config"]["password_type"] == "text" ) {
					$data["password"] = hash( 'sha1', $data["password"] );
				}
			}
		
			$this->return_data .= "<p>Trying to add: " . $data["email"] . "</p>";
			$this->_validate_member( $data );
			$errors = FALSE;
			if( count( $this->errors ) ) {
				$errors = TRUE;
				foreach( $this->errors as $errors ) {
					foreach( $errors as $error ) {
						$this->return_data .= "<p>&nbsp;&nbsp;&nbsp;&nbsp;" . $error . "</p>";
					}
				}
				$this->return_data .= "<p>Member " . $data["email"] . " NOT created</p>";
			}
		
			// Add member
			// $member_id = $this->_member_exists( $data );
			
			if( $errors === FALSE ) {

				// Add extra standard member fields
				$base_fields = array(
					'url', 'location', 'occupation', 'interests', 'aol_im',
					'icq', 'yahoo_im', 'msn_im', 'bio');
				foreach( $base_fields as $field ) {
					if( $datatype->get_item( $item, $this->settings["config"][ $field ] ) != "" ) {
						$data[ $field ] = $datatype->get_item( $item, $this->settings["config"][ $field ] );
					}
				}

				$data['unique_id'] = random_string('encrypt');
				$data['ip_address'] = '0.0.0.0';
				$data['join_date'] = $this->localize->now;				

				// Map data to custom member fields
				$cust_data = array();
				foreach( $this->settings["config"] as $field => $column ) {
					if( substr( $field, 0, 10 ) == "m_field_id" ) {
						if( $datatype->get_item( $item, $this->settings["config"][ $field ] ) != "" ) {
							$cust_data[ $field ] = $datatype->get_item( $item, $this->settings["config"][ $field ] );
						}
					}
				}

				// print_r( $cust_data );

				$member_id = $this->_create_member( $data, $cust_data );
				
				$this->return_data .= "<p>New member " . $data["email"] . " created</p>";
				
			}	
		}
		
		return $this->return_data;
	}
	
	function _validate_member( $data ) {
		
		$this->errors = array();
		
		$this->load->library('validate');
		$this->load->helper('security');
		
		$this->validate->member_id			= '';
		$this->validate->val_type			= 'new';
		$this->validate->fetch_lang			= TRUE;
		$this->validate->require_cpw		= FALSE;
		$this->validate->enable_log			= FALSE;
		$this->validate->cur_username		= '';
		$this->validate->cur_screen_name	= '';
		$this->validate->cur_password		= '';
		$this->validate->cur_email			= '';
		
		$username = $this->validate->username = $data["username"];
		$screen_name = $this->validate->screen_name = $data["screen_name"];
		$email = $this->validate->email = $data["email"];
		
		$this->validate->validate_username();

		if ( ! empty($this->validate->errors))
		{
			foreach($this->validate->errors as $key => $val)
			{
				$this->validate->errors[$key] = $val . ": " . $username;
			}
			$this->errors[] = $this->validate->errors;
			unset($this->validate->errors);
		}

		$this->validate->validate_screen_name();

		if ( ! empty($this->validate->errors))
		{
			foreach($this->validate->errors as $key => $val)
			{
				$this->validate->errors[$key] = $val . ": '" . $screen_name . "'";
			}
			$this->errors[] = $this->validate->errors;
			unset($this->validate->errors);
		}

		$this->validate->validate_email();

		if ( ! empty($this->validate->errors))
		{
			foreach($this->validate->errors as $key => $val)
			{
				$this->validate->errors[$key] = $val . ": " . $email;
			}
			$this->errors[] = $this->validate->errors;
			unset($this->validate->errors);
		}
		
	}
	
	/**
	 * Create new member and fills custom member fields
	 *
	 * @return void
	 * @author Andrew Weaver
	 */
	function _create_member( $data, $cust_fields=FALSE ) {

		$this->load->model('member_model');
		// $this->load->helper('security');

		// Add the additional fields

		if( !isset( $data["ip_address"] )) {
			// $data['ip_address']	= $this->input->ip_address();
			$data['ip_address'] = '0.0.0.0';
		}
		if( !isset( $data["unique_id"] )) {
			$data['unique_id']	= random_string('encrypt');
		}
		if( !isset( $data["join_date"] )) {
			$data['join_date']	= $this->localize->now;
		}
		$data['language'] 	= $this->config->item('deft_lang');
		$data['timezone'] 	= ($this->config->item('default_site_timezone') && $this->config->item('default_site_timezone') != '') ? $this->config->item('default_site_timezone') : $this->config->item('server_timezone');
		// $data['daylight_savings'] = ($this->config->item('default_site_dst') && $this->config->item('default_site_dst') != '') ? $this->config->item('default_site_dst') : $this->config->item('daylight_savings');
		$data['time_format'] = ($this->config->item('time_format') && $this->config->item('time_format') != '') ? $this->config->item('time_format') : 'us';

		if( !isset( $data["group_id"] )) {
			$data['group_id'] = 5; // Members
		}

		$member_id = $this->member_model->create_member($data, $cust_fields);
		
		return $member_id;
	}
	

}

/* End of file simple_model.php */
/* Location: /system/expressionengine/third_party/ajw_import_members/models/ajw_import_members_model.php */