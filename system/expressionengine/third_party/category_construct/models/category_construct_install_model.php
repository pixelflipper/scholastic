<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Category Construct Model
 *
 * @package    category_construct
 * @author     TJ Draper <tj@buzzingpixel.com>
 * @link       https://buzzingpixel.com/ee-add-ons/category-construct
 * @copyright  Copyright (c) 2015, BuzzingPixel
 */
class Category_construct_install_model extends CI_Model {

	/*------------------------------------*\
		# Public Methods
	\*------------------------------------*/

	/**
	 * Constructor
	 *
	 * @access      public
	 */
	public function __construct()
	{
		ee()->load->dbforge();
	}

	/**
	 * Insert Category Construct Settings Table
	 *
	 * @access      private
	 * @return      bool
	 */
	public function insertSettingsTable()
	{
		$fields = array(
			'settings_key' => array(
				'type' => 'TINYTEXT'
			),
			'settings_value' => array(
				'type' => 'TEXT'
			)
		);

		ee()->dbforge->add_field($fields);

		ee()->dbforge->create_table('category_construct_settings', true);

		$data = array(
			array(
				'settings_key' => 'phone_home',
				'settings_value' => 0
			),
			array(
				'settings_key' => 'license_key',
				'settings_value' => null
			)
		);

		ee()->db->insert_batch('category_construct_settings', $data);

		return true;
	}

	/**
	 * Remove Category Construct Settings Table
	 *
	 * @access      public
	 * @return      bool
	 */
	public function removeSettingsTable()
	{

		ee()->dbforge->drop_table('category_construct_settings');

		return true;
	}
}